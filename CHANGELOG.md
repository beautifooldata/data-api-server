# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1 Aug 2023]

### Added
- Created `/event-image` endpoint, to show MoonCats wearing owned accessories for the current event
- Created `/events` endpoint, to list known events that have specific accessories

## [10 May 2023]

### Changed
- Updated ownership snapshots

## [6 Mar 2023]

### Added
- Created `/owned-mooncats/{ETH_ADDRESS}` endpoint, to enumerate all MoonCats owned by a specific address.
- Added `Cache-Control` and `Age` headers to API response values.

### Changed
- Updated `/contract-details/{MOONCAT_ID}` endpoint to identify MoonCats traveling in the JumpPort.

## [26 Feb 2023]

### Fixed
- Fix crash if trying to delete cache file that's already deleted

## [25 Feb 2023]

### Fixed
- "Dynamic" view for MoonCats with no accessories

## [24 Feb 2023]

### Changed
- "Dynamic" MoonCat view (HTML used as `animation_url` target) defaults to accessories off
- "Total Accessories" and individual Accessory traits removed from `attributes` (but remain in `details` for other implementations to use)
### Fixed
- Ensured all endpoints have a check to see if headers have already been sent before finalizing (to handle the case where a "timeout" response has already been sent)

## [22 Feb 2023]

### Added
- File-caching mechanism for images
### Changed
- Updated MoonCat metadata to point to `/cat-image` as the default image for that asset.
- "Hue" and "Total Accessories" traits marked as numeric

## [17 Jan 2023]

### Changed
- Accessories marked as "costumes" updated to include updates from MEAO.

## [4 Jan 2023]

### Fixed
- Fix error-handling for requests with no identifer (e.g. `/costumes` endpoint).

## [3 Jan 2023]

### Added
- Request timeout logic, to return a specific response to users if a request is taking too long (likely delays in querying blockchain data)
- Convenience endpoints for requesting different preset MoonCat images without additional query parameters.

### Fixed
- Allow requeust identifiers to end in `.png` or `.json`.

## [14 Dec 2022]

### Changed
- Increased requests-per-minute limit.

## [9 Dec 2022]

### Changed
- Accessories marked as "costumes" updated to include updates from MEAO.

### Fixed
- Default glow for `/image` responses fixed to be the MoonCat's Acclimation status, and sized well by default at small scales.
- Have badly-formatted ID requests return 400 errors rather than 500 errors.

## [2 Dec 2022]
### Added
- Rate-limiting logic, to mitigate the issue of individual users using up large amounts of server resources attempting to iterate through the whole collection rapidly.

### Changed
- Caching infrastructure to handle asynchronous item insertion more robustly, and to fail back to old data if fetching new data fails.

## [22 Oct 2022]

### Fixed
- Add traits for un-rescued ("lunar" and "hero") MoonCats.

## [3 Oct 2022]

### Added
- Ability to visualize just the head/face of a MoonCat.
- Categorization of "costume" accessories and ability to specify rendering them or not.

### Changed
- Back-end infrastructure changed to an ExpressJS engine.