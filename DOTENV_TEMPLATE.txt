# Below is an example of the contents of your ENV file.
# You should create a `.env` file for local dev
API_URL="https://eth-mainnet.alchemyapi.io/v2/API_KEY_HERE"
BASE_URL="http://localhost:25690"
CONFIG_FILENAME="config.json"