# MoonCat Data API Server

A centralized server to make interacting with MoonCatRescue ecosystem projects easier. This project fetches blockchain data, and transforms it into readily-parsed formats. It provides multiple ways to generate imagery of MoonCats and Accessories.

For documentation on the endpoints themselves, see the documentation at https://api.mooncat.community, or the OpenAPI specification at [`spec.yml`](/spec.yml) (paste the contents of that file into https://editor-next.swagger.io/ for a more human-fiendly version of that specification).

The overall architecture of the API infrastructure is changing over to a new host and endpoint layout. The new API endpoints can be found at https://api.mooncatrescue.com, and full OpenAPI [documentation here](https://api.mooncatrescue.com/docs.html).

To view updates for this server, take a look at the [`CHANGELOG.md`](/CHANGELOG.md) file.

# Development Environment

To run this application server locally:

- Copy the `DOTENV_TEMPLATE.txt` file to `.env`
- Edit `.env` and replace the `API_URL` value with your own URL to an Ethereum node (sign up for a free [Alchemy](https://alchemy.com/?r=6cccf5016cfcb3ed) or [Infura](https://www.infura.io/) account if you don't have one).
- Run `docker-compose build web`
- Run `docker-compose up web`

This will start a local instance of the Data API server, running on port 25690. You should therefore be able to access it at `http://localhost:25690`.

The server does not live-reload on code changes; if you change any of the source code, you'll need to `CTRL+C` to stop the current instance, re-run `docker-compose build`, and then `docker-compose up` again.

## Deploy to Heroku

The production instance of this application server is hosted on Heroku. Project maintainers can use the Heroku CLI to deploy new versions. To get a working environment for the Heroku CLI scripts within Docker, run:

```
docker pull ubuntu
docker build --no-cache -f Dockerfile.heroku -t heroku-cli .
docker run --rm -it -v ${PWD}:/app -v ${PWD}/.netrc:/root/.netrc -v /var/run/docker.sock:/var/run/docker.sock -w /app heroku-cli
```

Then within that container, to deploy an updated version of the application:

```
$ heroku login -i
$ heroku container:login
$ heroku container:push web --app mooncat-data-api
$ heroku container:release web --app mooncat-data-api
```

# Firebase transition

The `/firebase` folder is a sub-project that creates a version of the Data API server for hosting on the Google Firebase platform. The goal of this sub-project is to eventually replace the Heroku deployment. It's not a feature-complete match for the Heroku version yet, but is starting to replace functionalinty for individual API endpoints.

Firebase as a hosting platform provides integrated solutions for storage (Firestore) and "serverless" (Cloud Function) execution. The Heroku deployment is designed to be generally stateless except for some caching of data. This means when a request comes in for data, it needs to check the blockchain to see if the cached data it has is still relevant. The goal of the Firebase implementation is for it to have its own state (stored in the Firestore data structure) that keeps the blockchain data it needs. With that setup, clients requesting data can be a separate process from updating the data from on-chain, and can know more precisely if any cached data has changed or not.

## Firebase emulators

For local development, the Firebase Emulator suite can be used to emulate the Firebase environment. Because the whole Emulator Suite runs in-memory, it's not as performant as the real Firebase environments, especially dealing with large datasets. Hence for local develompent most of the time developers would be best only seeding their environments with a portion of the MoonCat ecosystem data.

```
docker-compose build --no-cache firebase
docker-compose up firebase -d
```

That will bring up a Docker container running the Firebase Emulator Suite. Visit http://localhost:29980 to see the Emulator Suite's web console.

| Command                                                                           | Action                                                    |
| --------------------------------------------------------------------------------- | --------------------------------------------------------- |
| `docker-compose exec firebase firebase -V`                                        | Check the version of the Firebase tools installed         |
| `docker-compose exec firebase npm run --prefix functions build`                   | Rebuild Typescript sources into ready-to-deploy Functions |
| `docker-compose exec firebase firebase emulators:export /app/docker-share/export` | Export a snapshot of the current Firestore emulated data  |
| `docker-compose exec firebase tail firestore-debug.log`                           | View the debugging log                                    |

The scripts in the `/firebase/functions/script` folder that start with "seed" in their name bring blockchain data into Firestore to be referenced by other processes. To run them against the Emulator Suite, the syntax is:

```
FIRESTORE_EMULATOR_HOST="localhost:25613" node firebase/functions/script/1_seed_mooncat_traits
```

To send seed data to a real Firebase environment, you'll need to get credentials for a "service account" in the environment you want to deploy to. Follow [the instructions here](https://firebase.google.com/docs/admin/setup#initialize_the_sdk_in_non-google_environments) to generate and download a private key file (put it in the `firebase/creds/` folder, which is marked to be ignored in Git). Then you can run:
`GOOGLE_APPLICATION_CREDENTIALS=creds/mykey.json node functions/script/1_seed_mooncat_traits` to have it authenticate and update the seed data.

## Firebase deploy

From within a `node:mooncat` environment:

```
firebase login --no-localhost
firebase deploy --only functions
firebase deploy --only hosting
```
