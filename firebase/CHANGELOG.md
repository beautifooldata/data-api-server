# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [26 Aug 2023]

### Added
- `GenesisCatsAdded` Events added to logged blockchain events.

## [31 July 2023]

### Added
- API endpoint `/events/` for querying blockchain events related to MoonCatRescue projects.

## [7 July 2023]

### Added
- Initial Firebase infrastructure. This includes the ability to run the Firebase Emulator Suite in a Docker container, and setup/seeding scripts to create working staging and development environments
- Created cloud functions implementation of `/owned-mooncats/` API endpoint that returns the same data as the existing hosted version
- Created initial cloud function implementation of `/mooncat/traits/` API endpoint. It is not identical to the existing API trait endpoint on the hosted version, and mainly serves as a debugging mechanism.
- Created Firestore data structures for tracking MoonCat traits, individual addresses as MoonCat owners, and blockchain Event data
