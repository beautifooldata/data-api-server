require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore, Timestamp } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const queue = require('./lib/queue')
const { doMulticall } = require('./lib/contractEventsUtil')
const { rawNameToString } = require('../lib/moonCatUtils')

const { MoonCatRescue, MoonCatAcclimator, JumpPort } = require('@mooncatrescue/contracts/moonCatUtils')

const { API_URL } = process.env
if (API_URL == '') {
  console.error('No RPC API endpoint set')
  process.exit(1)
}
const provider = new ethers.providers.JsonRpcProvider(API_URL)

const MOONCAT_START_AT = 0
const TRAITS = new ethers.Contract(
  '0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2',
  [
    'function traitsOf (uint256 rescueOrder) public view returns (bool genesis, bool pale, string memory facing, string memory expression, string memory pattern, string memory pose, bytes5 catId, uint16 rescueYear, bool isNamed)',
    'function nameOf (uint256 rescueOrder) public view returns (string name)',
  ],
  provider
)

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

function sleep(delay) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, delay * 1000)
  })
}

/**
 * Generator function for adding MoonCat data to Firebase
 *
 * For use in proccessing a queue; yields promises that resolve
 * when the data is in Firebase successfully.
 *
 * Note, the resolution of the returned promise means the individual update
 * of the document is done. Any post-update hooks that get triggered from that
 * document being updated still need to be run on the server.
 */
function* moonCatDataGenerator(docs) {
  async function addDoc(doc) {
    //console.log(doc)
    try {
      let rs = await db.collection('mooncats').doc(doc.catId).set(doc)
      return {
        output: rs,
        input: doc,
      }
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  }

  for (let [index, doc] of docs.entries()) {
    //console.log(index, doc.rescueOrder, doc.catId)
    //if (index > 0 && index % 10 == 0) console.log(`At queue ${index}...`)
    yield addDoc(doc)
  }
}

/**
 * Query MoonCat trait data from the blockchain and insert into Firebase
 *
 * Use the MoonCats on-chain contract to enumerate human-friendly labels for
 * each MoonCat, and query the MoonCatRescue, Acclimator, and JumpPort contracts
 * to record who owns that MoonCat at the moment.
 */
;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const latestBlock = await provider.getBlock('latest')
  console.log('latest block', latestBlock.number)
  const checked = {
    blockHeight: latestBlock.number,
    timestamp: Timestamp.fromMillis(latestBlock.timestamp * 1000),
  }

  // Insert MoonCat trait data into the “mooncats” collection
  console.log('Adding MoonCats...')
  const CHUNK_SIZE = 50
  for (let i = MOONCAT_START_AT; i < 25440; i += CHUNK_SIZE) {
    console.log(`At MoonCat ${i}...`)
    let calls = []
    for (let j = 0; j < CHUNK_SIZE; j++) {
      const rescueOrder = i + j
      if (rescueOrder >= 25440) break
      calls.push({
        id: 'traits',
        index: j,
        contract: TRAITS,
        method: 'traitsOf',
        args: [rescueOrder],
      })
      calls.push({
        id: 'acclimated-owner',
        index: j,
        contract: MoonCatAcclimator,
        method: 'ownerOf',
        args: [rescueOrder],
      })
      calls.push({
        id: 'jumpport-owner',
        index: j,
        contract: JumpPort,
        method: 'ownerOf',
        args: [MoonCatAcclimator.address, rescueOrder],
      })
    }
    let output = []
    let subCalls = []
    for (let rs of await doMulticall(calls)) {
      switch (rs.id) {
        case 'traits':
          output[rs.index] = {
            rescueOrder: rs.index + i,
            catId: rs.parsed.catId,
            genesis: rs.parsed.genesis,
            pale: rs.parsed.pale,
            facing: rs.parsed.facing,
            expression: rs.parsed.expression,
            pattern: rs.parsed.pattern,
            pose: rs.parsed.pose,
            rescueYear: rs.parsed.rescueYear,
            name: {
              checked: checked,
              value: false,
              isNamed: rs.parsed.isNamed,
            },
            owner: {},
          }
          subCalls.push({
            id: 'rescue-owner',
            index: rs.index,
            contract: MoonCatRescue,
            method: 'catOwners',
            args: [rs.parsed.catId],
          })
          if (rs.parsed.isNamed) {
            subCalls.push({
              id: 'name-raw',
              index: rs.index,
              contract: MoonCatRescue,
              method: 'catNames',
              args: [rs.parsed.catId],
            })
          }
          break
        case 'acclimated-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index].owner[MoonCatAcclimator.address] = {
              value: false,
              checked: checked,
            }
          } else {
            output[rs.index].owner[MoonCatAcclimator.address] = {
              value: rs.parsed[0],
              checked: checked,
            }
          }
          break
        case 'jumpport-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index].owner[JumpPort.address] = {
              value: false,
              checked: checked,
            }
          } else {
            output[rs.index].owner[JumpPort.address] = {
              value: rs.parsed[0],
              checked: checked,
            }
          }
          break
        default:
          console.error('Unknown result ID', rs)
          process.exit(1)
      }
    }

    for (let rs of await doMulticall(subCalls)) {
      switch (rs.id) {
        case 'name-raw':
          output[rs.index].name.nameRaw = rs.parsed[0]
          output[rs.index].name.value = rawNameToString(rs.parsed[0])
          break
        case 'rescue-owner':
          output[rs.index].owner[MoonCatRescue.address] = {
            value: rs.parsed[0],
            checked: checked,
          }
          break
        default:
          console.error('Unknown result ID', rs)
          process.exit(1)
      }
    }

    let t = sleep(CHUNK_SIZE / 2)
    await queue(moonCatDataGenerator(output))
    await t // Make sure script doesn't run faster than prescribed rate, to allow Firestore post-update hooks to not get backlogged
  }
  console.log('Done!')
})()
