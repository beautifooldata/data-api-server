require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const { processEventLog, CONFIG_COLLECTION, CONFIG_DOC } = require('../lib/logUtils')
const { getEventsForContract, getStartBlock } = require('./lib/contractEventsUtil')
const { MoonCatAcclimator } = require('@mooncatrescue/contracts/moonCatUtils')

const { API_URL } = process.env
const provider = new ethers.providers.JsonRpcProvider(API_URL)

const ACCLIMATOR = MoonCatAcclimator.connect(provider)
const START_BLOCK = 12287196

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

function markCompletedHeight(eventName, blockHeight) {
  let keyName = `${MoonCatAcclimator.address}-${eventName}`
  return db
    .collection(CONFIG_COLLECTION)
    .doc(CONFIG_DOC)
    .set({ [keyName]: blockHeight }, { merge: true })
}

;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  // Insert MoonCatAcclimator Events into the EVENTS_COLLECTION collection
  const MAX_EVENTS = IS_EMULATED ? 500 : 5_000
  for (eventName of ['Transfer']) {
    const startBlock = await getStartBlock(`${MoonCatAcclimator.address}-${eventName}`, db, START_BLOCK)
    console.log(`Adding ${eventName} events from ${startBlock}...`)
    let lastBlockHeight = startBlock

    // Log processing needs to happen in order. This process does not use a queue for parallel execution
    // because if there are multiple Events in the same transaction, the last Event should be the one that
    // is marked as the "last-modified" value. If Events are processed in parallel, there's a chance of a race
    // condition. Hence each Event is committed to the data store before moving on to process the next Event.
    for await (const { log, index } of getEventsForContract(ACCLIMATOR, startBlock, eventName, MAX_EVENTS)) {
      lastBlockHeight = log.blockNumber

      await processEventLog(db, ACCLIMATOR, log)

      if (index > 0 && index % 10 == 0) console.log(`At ${index}...`)
      if (index > 0 && index % 50 == 0) {
        await markCompletedHeight(eventName, log.blockNumber)
      }
    }
    await markCompletedHeight(eventName, lastBlockHeight)
    console.log('Done!')
  }
})()
