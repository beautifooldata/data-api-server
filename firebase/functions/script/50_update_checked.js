require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore, Timestamp } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const queue = require('./lib/queue')
const { doMulticall } = require('./lib/contractEventsUtil')
const { rawNameToString, MOONCAT_COLLECTION } = require('../lib/moonCatUtils')

const { MoonCatRescue, MoonCatAcclimator, JumpPort } = require('@mooncatrescue/contracts/moonCatUtils')

const { API_URL } = process.env
if (API_URL == '') {
  console.error('No RPC API endpoint set')
  process.exit(1)
}
const provider = new ethers.providers.JsonRpcProvider(API_URL)

const MOONCAT_START_AT = 0

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

function sleep(delay) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, delay * 1000)
  })
}

/**
 * Generator function for updating MoonCat data to Firebase
 *
 * For use in proccessing a queue; yields promises that resolve
 * when the data is in Firebase successfully.
 */
function* moonCatDataGenerator(docs) {
  async function addDoc(doc) {
    //console.log(doc)
    try {
      let rs = await db.collection('mooncats').doc(doc.catId).update(doc)
      return {
        output: rs,
        input: doc,
      }
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  }

  for (let [index, doc] of docs.entries()) {
    //console.log(index, doc.rescueOrder, doc.catId)
    //if (index > 0 && index % 10 == 0) console.log(`At queue ${index}...`)
    yield addDoc(doc)
  }
}

/**
 * Query MoonCat trait data from the blockchain and update the data in Firebase
 *
 * This script iterates through all the MoonCats the Firestore database knows of, and checks the current status of
 * traits that can change (ownership and named status).
 */
;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const latestBlock = await provider.getBlock('latest')
  console.log('latest block', latestBlock.number)
  const checked = {
    blockHeight: latestBlock.number,
    timestamp: Timestamp.fromMillis(latestBlock.timestamp * 1000),
  }

  console.log('Updating MoonCats...')
  const CHUNK_SIZE = 10
  // Find the MoonCat to start at
  let mcSnap = await db.collection(MOONCAT_COLLECTION).where('rescueOrder', '<=', MOONCAT_START_AT).orderBy('rescueOrder', 'desc').limit(3).get()
  if (mcSnap.empty) {
    console.log(mcSnap)
    console.error('Cannot find MoonCat with rescue order of', MOONCAT_START_AT)
    process.exit(1)
  }
  let moonCatCursor = mcSnap.docs[0]
  const query = db.collection(MOONCAT_COLLECTION).orderBy('rescueOrder').limit(CHUNK_SIZE)
  let chunk = await query.startAt(moonCatCursor).get() // First chunk is starting AT the cursor

  while (!chunk.empty) {
    console.log(`At MoonCat ${moonCatCursor.get('rescueOrder')}...`)
    let output = []
    let calls = []
    for (let index in chunk.docs) {
      const ref = chunk.docs[index]
      output[index] = { catId: ref.get('catId') }
      calls.push({
        id: 'name',
        index: index,
        contract: MoonCatRescue,
        method: 'catNames',
        args: [ref.get('catId')],
      })
      calls.push({
        id: 'rescue-owner',
        index: index,
        contract: MoonCatRescue,
        method: 'catOwners',
        args: [ref.get('catId')],
      })
      calls.push({
        id: 'acclimated-owner',
        index: index,
        contract: MoonCatAcclimator,
        method: 'ownerOf',
        args: [ref.get('rescueOrder')],
      })
      calls.push({
        id: 'jumpport-owner',
        index: index,
        contract: JumpPort,
        method: 'ownerOf',
        args: [MoonCatAcclimator.address, ref.get('rescueOrder')],
      })
    }
    for (let rs of await doMulticall(calls)) {
      switch (rs.id) {
        case 'name':
          if (rs.parsed[0] == '0x0000000000000000000000000000000000000000000000000000000000000000') {
            output[rs.index][`name.isNamed`] = false
            output[rs.index][`name.value`] = false
            output[rs.index][`name.checked`] = checked
          } else {
            output[rs.index][`name.isNamed`] = true
            output[rs.index][`name.nameRaw`] = rs.parsed[0]
            output[rs.index][`name.value`] = rawNameToString(rs.parsed[0])
            output[rs.index][`name.checked`] = checked
          }
          break
        case 'rescue-owner':
          output[rs.index][`owner.${MoonCatRescue.address}.checked`] = checked
          output[rs.index][`owner.${MoonCatRescue.address}.value`] = rs.parsed[0]
          break
        case 'acclimated-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index][`owner.${MoonCatAcclimator.address}.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}.value`] = false
          } else {
            output[rs.index][`owner.${MoonCatAcclimator.address}.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}.value`] = rs.parsed[0]
          }
          break
        case 'jumpport-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index][`owner.${JumpPort.address}.checked`] = checked
            output[rs.index][`owner.${JumpPort.address}.value`] = false
          } else {
            output[rs.index][`owner.${JumpPort.address}.checked`] = checked
            output[rs.index][`owner.${JumpPort.address}.value`] = rs.parsed[0]
          }
          break
      }
    }

    let t = sleep(CHUNK_SIZE / 5)
    await queue(moonCatDataGenerator(output))
    await t // Make sure script doesn't run faster than prescribed rate, to allow Firestore post-update hooks to not get backlogged

    // Grab next chunk
    moonCatCursor = chunk.docs[chunk.docs.length - 1]
    chunk = await query.startAfter(moonCatCursor).get() // Subsequent chunks start AFTER the cursor
  }

  console.log('Done!')
})()
