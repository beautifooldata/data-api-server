require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore, FieldValue, Timestamp } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const queue = require('./lib/queue')
const { doMulticall } = require('./lib/contractEventsUtil')
const { rawNameToString } = require('../lib/moonCatUtils')

const { MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils')

const { API_URL } = process.env
if (API_URL == '') {
  console.error('No RPC API endpoint set')
  process.exit(1)
}
const provider = new ethers.providers.JsonRpcProvider(API_URL)

const RESCUE = MoonCatRescue.connect(provider)
const TRAITS = new ethers.Contract(
  '0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2',
  [
    'function traitsOf (uint256 rescueOrder) public view returns (bool genesis, bool pale, string memory facing, string memory expression, string memory pattern, string memory pose, bytes5 catId, uint16 rescueYear, bool isNamed)',
    'function nameOf (uint256 rescueOrder) public view returns (string name)',
  ],
  provider
)

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

/**
 * Generator function for adding MoonCat data to Firebase
 *
 * For use in proccessing a queue; yields promises that resolve
 * when the data is in Firebase successfully.
 *
 * Note, the resolution of the returned promise means the individual update
 * of the document is done. Any post-update hooks that get triggered from that
 * document being updated still need to be run on the server.
 */
function* moonCatDataGenerator(docs) {
  async function updateDoc(doc) {
    //console.log(doc)
    try {
      let rs = await db.collection('mooncats').doc(doc.catId).update(doc)
      return {
        output: rs,
        input: doc,
      }
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  }

  for (let [index, doc] of docs.entries()) {
    //console.log(index, doc.rescueOrder, doc.catId)
    //if (index > 0 && index % 10 == 0) console.log(`At queue ${index}...`)
    yield updateDoc(doc)
  }
}

/**
 * Fix structure of MoonCat trait documents
 *
 * This script serves as an example of a maintenance/migration script, where the structure of a type of Firestore document needs to change.
 *
 * This script finds MoonCat trait documents that have an `isNamed` property at the top level, and changes
 * them to instead have a `name` property that adheres to the ADR0007 blockchain storage architecture.
 */
;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const latestBlock = await provider.getBlock('latest')
  console.log(latestBlock.number)
  const checked = {
    blockHeight: latestBlock.number,
    timestamp: Timestamp.fromMillis(latestBlock.timestamp * 1000),
  }

  while (true) {
    // Search for MoonCat documents with bad name structure
    docsToFix = await db
      .collection('mooncats')
      .where('isNamed', 'in', [false, true])
      .orderBy('rescueOrder')
      .limit(50)
      .get()

    if (docsToFix.empty) {
      console.log('No documents to fix')
      process.exit(0)
    }

    console.log(`At MoonCat ${docsToFix.docs[0].get('catId')} (${docsToFix.docs[0].get('rescueOrder')})`)

    let calls = []
    docsToFix.docs.forEach((doc, index) => {
      calls.push({
        id: 'traits',
        index: index,
        contract: TRAITS,
        method: 'traitsOf',
        args: [doc.get('rescueOrder')],
      })
      calls.push({
        id: 'name-raw',
        index: index,
        contract: RESCUE,
        method: 'catNames',
        args: [doc.get('catId')],
      })
    })

    let output = []
    for (let rs of await doMulticall(calls)) {
      switch (rs.id) {
        case 'traits':
          output[rs.index] = {
            catId: docsToFix.docs[rs.index].get('catId'),
            isNamed: FieldValue.delete(), // Remove the old structure
            // Add in the new structure
            name: {
              checked: checked,
              value: null,
              isNamed: rs.parsed.isNamed,
            },
          }
          break
        case 'name-raw':
          if (output[rs.index].name.isNamed === false) break
          output[rs.index].nameRaw = FieldValue.delete() // Remove the old structure
          output[rs.index].name.nameRaw = rs.parsed[0] // Add in the new structure
          output[rs.index].name.value = rawNameToString(rs.parsed[0])
          break
        default:
          console.error('Unknown result ID', rs)
          process.exit(1)
      }
    }

    await queue(moonCatDataGenerator(output))
  }
})()
