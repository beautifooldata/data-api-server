require('dotenv').config()
const { ethers } = require('ethers')
const { CONFIG_COLLECTION, CONFIG_DOC } = require('../../lib/logUtils')

const { API_URL } = process.env
if (API_URL == '') {
  console.error('No RPC API endpoint set')
  process.exit(1)
}
const provider = new ethers.providers.JsonRpcProvider(API_URL)

const CHUNK_SIZE = 20000

const MULTI = new ethers.Contract(
  '0xcA11bde05977b3631167028862bE2a173976CA11',
  [
    // https://github.com/mds1/multicall
    'function aggregate3(tuple(address target, bool allowFailure, bytes callData)[] calls) external view returns (tuple(bool success, bytes returnData)[] returnData)',
  ],
  provider
)

/**
 * Generator function that yields event logs from a specific contract for a specific event name
 * @param {ethers.Contract} contract Ethers Contract object to be queried
 * @param {number} startBlock Block to start querying logs at
 * @param {string} eventName Name of the event to filter for
 * @param {number} limit Maximum number of events to process
 */
const getEventsForContract = async function* (contract, startBlock, eventName, limit) {
  let curBlock = startBlock
  let latestBlock = await contract.provider.getBlock('latest')
  let index = 0
  while (curBlock < latestBlock.number && index <= limit) {
    let logs = await contract.queryFilter(eventName, curBlock, curBlock + CHUNK_SIZE)
    console.log(`Processing ${logs.length} events from block ${curBlock}...`)
    for (log of logs) {
      yield { log, index }
      index++
    }
    curBlock += CHUNK_SIZE
  }
}

const getStartBlock = async function (configKey, db, defaultStartBlock) {
  const config = await db.collection(CONFIG_COLLECTION).doc(CONFIG_DOC).get()
  return config.exists && config.get(configKey) ? config.get(configKey) - 1 : defaultStartBlock
}

/**
 * Make a batch call through public multicall contract
 */
async function doMulticall(calls) {
  let calldatas = []
  for (let call of calls) {
    let cd = (
      await call.contract.populateTransaction[call.method].apply(
        null,
        call.args
      )
    ).data
    calldatas.push({
      target: call.contract.address,
      allowFailure: true,
      callData: cd,
    })
  }
  let rs = await MULTI.aggregate3(calldatas)
  return rs.map((callRs, i) => {
    let out = {
      id: calls[i].id,
      index: calls[i].index,
      success: callRs.success,
      data: callRs.returnData,
    }
    if (callRs.success) {
      out.parsed = calls[i].contract.interface.decodeFunctionResult(
        calls[i].method,
        callRs.returnData
      )
    }
    return out
  })
}


module.exports = {
  getEventsForContract,
  getStartBlock,
  doMulticall
}
