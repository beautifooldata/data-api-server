/**
 * Promise pool job queue
 *
 * Given a Generator function that produces async jobs when called, iterate through them with a specified level of
 * parallel concurrency.
 * @param {function} gen Generator function instance
 * @param {number} concurrency Amount of jobs to process at once
 * @returns Promise that resolves to an array of results from the individual jobs, in the generated order
 */
module.exports = function queue(gen, concurrency) {
  return new Promise((resolve, reject) => {
    const done = []
    let numRunning = 0
    let curJob = 0
    let queueFinished = false

    const processResult = function (genRs, jobNum) {
      if (typeof genRs.value.then == 'function') {
        // Job is still processing
        genRs.value.then((data) => {
          // Async job is now done
          done[jobNum] = data
          numRunning--
          if (numRunning == 0 && queueFinished) {
            // Queue has finished processing
            resolve(done)
            return
          }
          doWork()
        })
      } else {
        // Job has resolved
        done[jobNum] = genRs.value
        numRunning--
        if (numRunning == 0 && queueFinished) {
          // Queue has finished processing
          resolve(done)
          return
        }
        doWork()
      }
    }

    const doWork = async function () {
      if (numRunning >= concurrency) return
      // Grab another job off the generator
      let genRs = gen.next(curJob)
      if (!genRs.value) {
        // Generator has reached the end of its listing
        queueFinished = true
        return
      }

      // Process the found job
      numRunning++
      if (typeof genRs.then == 'function') {
        // Async job; use IIFE to keep current job number in-scope
        ;((jobNum) => genRs.then((rs) => processResult(rs, jobNum)))(curJob)
      } else {
        processResult(genRs, curJob)
      }
      curJob++
      doWork()
    }
    doWork()
  })
}