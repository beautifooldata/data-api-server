import { ethers } from 'ethers'
import { initializeApp } from 'firebase-admin/app'
import { DocumentSnapshot, Filter, FieldValue, Timestamp, getFirestore } from 'firebase-admin/firestore'
import { MoonCatRescue, MoonCatAcclimator, JumpPort, OldWrappedMoonCats } from '@mooncatrescue/contracts/moonCatUtils'
import { ADDR_META_COLLECTION, EVENTS_COLLECTION, saveContractLogs } from './logUtils'
import { getOwnedMoonCats, MOONCAT_COLLECTION, MoonCatSummary, parseMoonCatIdentifier } from './moonCatUtils'
import { BlockchainValue, HexString } from './types'

/**
 * Create Firestore Cloud Functions
 *
 * This is the main entrypoint for creating Functions that will be deployed to Firebase.
 * All functions that are export from this file will be found by a `firebase deploy` call and turned
 * into Cloud Functions.
 */

// The Cloud Functions for Firebase SDK. Used to create Cloud Functions and set up triggers.
import { onRequest, Request } from 'firebase-functions/v2/https'
import { onDocumentWritten } from 'firebase-functions/v2/firestore'
import { logger } from 'firebase-functions'

// The Firebase Admin SDK to access Firestore.
initializeApp()
const db = getFirestore()

/**
 * Fetch the most-recent MoonCatRescue events and save them to Firebase
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const updateMoonCatRescueEvents = onRequest({ secrets: ['API_URL'], timeoutSeconds: 540 }, async (req, res) => {
  switch (req.method) {
    case 'GET':
      const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)
      const rs = await saveContractLogs({
        provider: provider,
        db: db,
        taskName: 'mooncatrescue-events-catchup',
        contract: MoonCatRescue,
      })
      res.status(rs.code).json(rs)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * Fetch the most-recent MoonCatAcclimator events and save them to Firebase
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const updateMoonCatAcclimatorEvents = onRequest(
  { secrets: ['API_URL'], timeoutSeconds: 540 },
  async (req, res) => {
    switch (req.method) {
      case 'GET':
        const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)
        const rs = await saveContractLogs({
          provider: provider,
          db: db,
          taskName: 'mooncatacclimator-events-catchup',
          contract: MoonCatAcclimator,
        })
        res.status(rs.code).json(rs)
        return
      case 'OPTIONS':
        res.setHeader('Allow', ['GET'])
        res.status(204).end()
        return
      default:
        res.setHeader('Allow', ['GET'])
        res.status(405).end()
        return
    }
  }
)

/**
 * Fetch the most-recent JumpPort events and save them to Firebase
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const updateJumpPortEvents = onRequest({ secrets: ['API_URL'], timeoutSeconds: 540 }, async (req, res) => {
  switch (req.method) {
    case 'GET':
      const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)
      const rs = await saveContractLogs({
        provider: provider,
        db: db,
        taskName: 'jumpport-events-catchup',
        contract: JumpPort,
      })
      res.status(rs.code).json(rs)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * Retrieve blockchain events.
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const getBlockchainEvents = onRequest(async (req, res) => {
  switch (req.method) {
    case 'GET':
      let limit = 10
      let contract: string | false = false
      let eventName: string | false = false
      let address: string | false = false
      let moonCatRescueOrder: number | false = false
      let moonCatHexId: string | false = false
      let startAfterBlock: number | false = false
      let startAfterLog: number | false = false

      // Check input parameters for errors
      let errors = []
      if (typeof req.query.limit != 'undefined') {
        if (
          typeof req.query.limit != 'string' ||
          isNaN(Number(req.query.limit)) ||
          parseInt(req.query.limit) < 1 ||
          parseInt(req.query.limit) > 500
        ) {
          errors.push('limit')
        } else {
          // Query parameter is valid; parse it
          limit = parseInt(req.query.limit)
        }
      }
      if (typeof req.query.contract != 'undefined') {
        if (typeof req.query.contract != 'string' || !ethers.utils.isAddress(req.query.contract)) {
          errors.push('contract')
        } else {
          // Query parameter is valid; parse it
          contract = ethers.utils.getAddress(req.query.contract)
        }
      }
      if (typeof req.query.event != 'undefined') {
        if (typeof req.query.event != 'string') {
          errors.push('event')
        } else {
          // Query parameter is valid; parse it
          eventName = req.query.event as string
        }
      }
      if (typeof req.query.address != 'undefined') {
        if (typeof req.query.address != 'string' || !ethers.utils.isAddress(req.query.address)) {
          errors.push('address')
        } else {
          // Query parameter is valid; parse it
          address = ethers.utils.getAddress(req.query.address)
        }
      }
      if (typeof req.query.mooncat != 'undefined') {
        if (typeof req.query.mooncat != 'string' || isNaN(Number(req.query.mooncat))) {
          errors.push('mooncat')
        } else {
          // String passed as MoonCat identifier to filter on; determine if it's a rescue order or hex ID, and look up the other
          const rs = await parseMoonCatIdentifier(db.collection(MOONCAT_COLLECTION), req.query.mooncat)
          moonCatRescueOrder = rs.rescueOrder
          moonCatHexId = rs.hexId
        }
      }

      if (typeof req.query.start_after != 'undefined') {
        if (typeof req.query.start_after != 'string') {
          errors.push('start_after')
        } else {
          // startAfter is a string; verify its format
          let [block, log] = req.query.start_after.split('-')
          if (block == '' || isNaN(Number(block)) || log == '' || isNaN(Number(log))) {
            errors.push('start_after')
          } else {
            // Query parameter is valid; parse it
            startAfterBlock = parseInt(block)
            startAfterLog = parseInt(log)
          }
        }
      }
      if (errors.length > 0) {
        res.status(400).json({ ok: false, error: 'Invalid ' + errors.join(', ') })
        return
      }

      // Query for events
      let query = db
        .collection(EVENTS_COLLECTION)
        .orderBy('blockNumber', 'desc')
        .orderBy('logIndex', 'desc')
        .limit(limit)

      // Limit to a specific smart contract's events if `contract` query param is set
      if (contract) {
        logger.debug(`Filter events for eventContract == ${contract}`)
        query = query.where('eventContract', '==', contract)
      }

      // Limit to a specific event name if `event` query param is set
      if (eventName) {
        logger.debug(`Filter events for eventName == ${eventName}`)
        query = query.where('eventName', '==', eventName)
      }

      // Limit to a specific address if `address` query param is set
      if (address) {
        logger.debug(`Filter events for address == ${address}`)
        query = query.where(
          Filter.or(
            Filter.where('tx.from', '==', address),
            Filter.where('tx.to', '==', address),
            Filter.where('args.sender', '==', address),
            Filter.where('args.account', '==', address),
            Filter.where('args.owner', '==', address),
            Filter.where('args.from', '==', address),
            Filter.where('args.to', '==', address)
          )
        )
      }

      // Limit to a specific MoonCat if `mooncat` query param is set
      if (moonCatRescueOrder !== false && moonCatHexId !== false) {
        // MoonCat rescue order and Hex ID are known; filter on all holding contracts
        logger.debug(`Filter events for MoonCat ${moonCatRescueOrder} (${moonCatHexId})`)
        query = query.where(
          Filter.or(
            Filter.and( // JumpPort-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('args.tokenAddress', '==', MoonCatAcclimator.address),
              Filter.where('eventContract', '==', JumpPort.address)
            ),
            Filter.and( // Acclimator-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('eventContract', '==', MoonCatAcclimator.address)
            ),
            Filter.and( // MoonCatRescue rescue events
              Filter.where('args.catId', '==', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            ),
            Filter.and( // MoonCatRescue Genesis batch events
              Filter.where('args.catIds', 'array-contains', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            )
          )
        )
      } else if (moonCatRescueOrder !== false) {
        // Only rescue order is known
        logger.debug(`Filter events for MoonCat ${moonCatRescueOrder}`)
        query = query.where(
          Filter.or(
            Filter.and( // JumpPort-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('args.tokenAddress', '==', MoonCatAcclimator.address),
              Filter.where('eventContract', '==', JumpPort.address)
            ),
            Filter.and( // Acclimator-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('eventContract', '==', MoonCatAcclimator.address)
            )
          )
        )
      } else if (moonCatHexId !== false) {
        // Only Hex ID is known
        logger.debug(`Filter events for MoonCat ${moonCatHexId}`)
        query = query.where(
          Filter.or(
            Filter.and( // MoonCatRescue rescue events
              Filter.where('args.catId', '==', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            ),
            Filter.and( // MoonCatRescue Genesis batch events
              Filter.where('args.catIds', 'array-contains', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            )
          )
        )
      }

      // Start at a specific offset, if `start_after` query param is set
      if (startAfterBlock) {
        logger.debug(`Filter event starting after ${startAfterBlock}-${startAfterLog}`)
        query = query.startAfter(startAfterBlock, startAfterLog)
      }

      // Return result
      const snapshot = await query.get()
      res.json(
        snapshot.docs.map((eventSnap) => {
          const data = eventSnap.data()
          data.timestamp = (data.timestamp as Timestamp).seconds
          return data
        })
      )
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * From a Firebase document that is a MoonCat metadata record, return that MoonCat's owner's address.
 *
 * To determine the owner is more complicated than just looking up a property on the document, since different staking/wrapping
 * contracts need to be accounted for.
 */
const getMoonCatOwnerFromDoc = function (moonCatDoc: DocumentSnapshot): BlockchainValue<HexString> | null {
  if (!moonCatDoc.exists) return null // Document doesn't actually exist in the Firebase store
  const moonCatData = moonCatDoc.data()
  if (typeof moonCatData == 'undefined') return null // Document exists, but has no data
  if (typeof moonCatData.owner === 'undefined') return null // Document exists, but has no ownership information

  if (moonCatData.owner[JumpPort.address]?.value !== false) {
    // JumpPort knows of this MoonCat; MoonCat is staked there and therefore what JumpPort reports as the owner is the true owner
    return moonCatData.owner[JumpPort.address]
  } else if (moonCatData.owner[MoonCatRescue.address]?.value == OldWrappedMoonCats.address) {
    // Old-wrapped MoonCat
    return moonCatData.owner[MoonCatRescue.address]
  } else if (moonCatData.owner[MoonCatAcclimator.address]?.value !== false) {
    // Acclimator knows of this MoonCat; MoonCat is Acclimated and therefore what the Acclimator reports as owner is the true owner
    return moonCatData.owner[MoonCatAcclimator.address]
  } else if (moonCatData.owner[MoonCatRescue.address]?.value !== false) {
    // The original MoonCatRescue contract's ownership info is the true owner
    return moonCatData.owner[MoonCatRescue.address]
  }

  // Corrupted document; none of the MoonCatRescue-related contracts have ownership info for this MoonCat
  throw new Error('Unknown owner')
}

/**
 * Whenever a MoonCat's metadata is updated, update ownership listing.
 *
 * This is a Firebase hook function that automatically fires whenever a Firestore document matching the criteria is updated.
 */
export const onMoonCatUpdated = onDocumentWritten(`/${MOONCAT_COLLECTION}/{moonCatId}`, async (event) => {
  if (typeof event == 'undefined' || typeof event.data == 'undefined') return null
  const moonCatId = event.params.moonCatId

  let newOwner: BlockchainValue<HexString> | null
  try {
    newOwner = getMoonCatOwnerFromDoc(event.data.after)
  } catch (err) {
    logger.error('Unknown owner', moonCatId, event.data.after.get('owner'))
    return null
  }

  let oldOwner: BlockchainValue<HexString> | null
  try {
    oldOwner = getMoonCatOwnerFromDoc(event.data.before)
  } catch (err) {
    logger.error('Unknown owner', moonCatId, event.data.before.get('owner'))
    return null
  }
  if (newOwner == null && oldOwner == null) return

  const oldOwnerMeta = oldOwner == null ? null : db.collection(ADDR_META_COLLECTION).doc(oldOwner.value)
  const newOwnerMeta = newOwner == null ? null : db.collection(ADDR_META_COLLECTION).doc(newOwner.value)

  const oldJumpPortOwner = event.data.before.get(`owner.${JumpPort.address}.value`)
  const newJumpPortOwner = event.data.after.get(`owner.${JumpPort.address}.value`)

  let doLeaseOldOwner = false
  let doLeaseNewOwner = false
  const logPreamble = `MoonCat document updated: ${moonCatId} from ${oldOwner?.value} -> ${newOwner?.value}`
  await db.runTransaction(async (dbtx) => {
    // Firestore transactions need to do all the data-reading first, before the data-writing
    if (oldOwnerMeta != null && oldOwner!.value !== newOwner?.value) {
      // Ownership value changed, and old value was a non-empty value; it should be updated
      let doc = await dbtx.get(oldOwnerMeta)
      const isDataMissing = !doc.exists || typeof doc.get('ownedMoonCats') == 'undefined'
      const isNotLeasedCurrently =
        typeof doc.get('lease') === 'undefined' || (doc.get('lease') as Timestamp).toMillis() < new Date().getTime()
      // Update this record if...
      if (isDataMissing || isNotLeasedCurrently) {
        // Record doesn't exist, or it does exist and no other process has it leased for pending updates, so this thread should
        doLeaseOldOwner = true
      }
    }
    if (newOwnerMeta != null) {
      // MoonCat has an owner value; check if they need updating
      let doc = await dbtx.get(newOwnerMeta)
      const isDataMissing = !doc.exists || typeof doc.get('ownedMoonCats') == 'undefined'
      const isNotLeasedCurrently =
        typeof doc.get('lease') === 'undefined' || (doc.get('lease') as Timestamp).toMillis() < new Date().getTime()
      if (isDataMissing) {
        // No existing document for this address; create it
        logger.info(`${logPreamble}, and ${newOwner!.value} has no metadata; creating.`)
        doLeaseNewOwner = true
      } else if (oldOwner?.value !== newOwner!.value) {
        // Ownership changed; the new owner should have their metadata updated
        if (isNotLeasedCurrently) {
          // No other process has a lease held on this record, so this thread should
          doLeaseNewOwner = true
        } else {
          logger.debug(`${logPreamble}, but other process is already updating`)
        }
      } else {
        // The MoonCat's document was updated, but the ownership value wasn't among the things updated
        if (
          (newJumpPortOwner == false && oldJumpPortOwner != false) ||
          (newJumpPortOwner != false && oldJumpPortOwner == false)
        ) {
          // JumpPort status changed; need to refresh where that MoonCat is held
          logger.debug(`${logPreamble}, via the JumpPort; updating ownership list for new status`)
          doLeaseNewOwner = true
        } else {
          logger.debug(`${logPreamble}, no change in ownership; no updates`)
        }
      }
    }

    // Lease the owner meta records for a minute
    const leaseTime = Timestamp.fromMillis(new Date().getTime() + 1000 * 60)
    if (doLeaseOldOwner) {
      await dbtx.set(oldOwnerMeta!, { lease: leaseTime }, { merge: true })
    }
    if (doLeaseNewOwner) {
      await dbtx.set(newOwnerMeta!, { lease: leaseTime }, { merge: true })
    }
  })

  if (!doLeaseOldOwner && !doLeaseNewOwner) {
    return
  }

  logger.debug(logPreamble)
  return await db.runTransaction(async (dbtx) => {
    const moonCatCollection = db.collection(MOONCAT_COLLECTION)
    // Firestore transactions need to do all the data-reading first, before the data-writing
    let updates = []
    if (doLeaseOldOwner) {
      // Refresh old owner's address metadata
      logger.debug(`Starting refresh of address MoonCat ownership metadata for ${oldOwner!.value}`)
      updates.push({
        address: oldOwner!.value,
        ownedMoonCats: await getOwnedMoonCats({ moonCatCollection, dbtx, targetAddress: oldOwner!.value }),
      })
    }
    if (doLeaseNewOwner) {
      // Refresh new owner's address metadata
      logger.debug(`Starting refresh of address MoonCat ownership metadata for ${newOwner!.value}`)
      updates.push({
        address: newOwner!.value,
        ownedMoonCats: await getOwnedMoonCats({ moonCatCollection, dbtx, targetAddress: newOwner!.value }),
      })
    }

    // Done reading updated info; write the updated info to the appropriate places
    for (let u of updates) {
      const newData = {
        ownedMoonCats: {
          list: u.ownedMoonCats,
          count: u.ownedMoonCats.length,
          modified: Timestamp.now(),
        },
        lease: FieldValue.delete(),
      }
      await dbtx.set(db.collection(ADDR_META_COLLECTION).doc(u.address), newData, { merge: true })
    }
  })
})

const getPathSuffix = function (req: Request, urlBase: string): string | undefined {
  let pos = req.path.indexOf(urlBase)
  if (pos < 0) {
    return undefined
  }
  return req.path.substring(pos + urlBase.length).trim()
}

/**
 * Retrieve trait information about a MoonCat.
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const getMoonCat = onRequest(async (req, res) => {
  switch (req.method) {
    case 'GET':
      let id = req.query.id
      if (!id || Array.isArray(id)) {
        // Fetch from path rather than query string
        id = getPathSuffix(req, '/mooncat/traits/')
        if (!id) {
          res.status(400).json({ ok: false, error: 'Bad input' })
          return
        }
      }
      logger.debug(`getMoonCat ID="${id}"`)
      let snapshot = await db
        .collection(MOONCAT_COLLECTION)
        .where('rescueOrder', '==', parseInt(id as string))
        .get()
      if (snapshot.empty) {
        snapshot = await db.collection('mooncats').where('catId', '==', id).get()
      }
      if (snapshot.empty) {
        res.status(404).json({ ok: false, error: 'Not found' })
      } else if (snapshot.docs.length > 1) {
        res.status(400).json({ ok: false, error: 'Too many found' })
      } else {
        res.json(snapshot.docs[0].data())
      }
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * Retrieve information about an address' owned MoonCats
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const getOwnedMoonCatsForAddress = onRequest({ secrets: ['API_URL'] }, async (req, res) => {
  switch (req.method) {
    case 'GET':
      const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)

      let address = req.query.address
      if (!address || Array.isArray(address)) {
        // Fetch from path rather than query string
        address = getPathSuffix(req, '/owned-mooncats/')
        if (!address) {
          res.status(400).json({ ok: false, error: 'Bad input' })
          return
        }
      }
      try {
        address = ethers.utils.getAddress((address as string).toLowerCase())
      } catch (err) {
        if ((err as any).code !== 'INVALID_ARGUMENT') {
          // Unknown/unexpected error
          logger.error('Ethers address parsing error', err)
          res.status(500).json({ ok: false, error: 'Parsing error' })
          return
        }
        // Expected parsing error for not an address. Try ENS resolution instead:
        let parsedAddress = await provider.resolveName(address as string)
        if (parsedAddress == null) {
          logger.debug(`Failed to parse '${address}' as an ENS address`)
          res.status(400).json({ ok: false, error: 'Bad address' })
          return
        }
        address = parsedAddress
      }

      let snapshot = await db
        .collection(ADDR_META_COLLECTION)
        .doc(address as string)
        .get()
      if (!snapshot.exists) {
        logger.info(`getOwnedMoonCats ADDRESS="${address}": address not found`)
        res.json([])
        return
      }
      const ownedMoonCats = snapshot.data()!.ownedMoonCats.list as MoonCatSummary[]
      logger.info(`getOwnedMoonCats ADDRESS="${address}": address owns ${ownedMoonCats.length} MoonCats`)
      res.json(ownedMoonCats)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})
