import { logger } from 'firebase-functions'
import { EventUpdateListener, shouldUpdateBlockchainMoment } from './logUtils'
import { BlockchainMoment, HexString } from './types'
import { JumpPort, MoonCatAcclimator } from '@mooncatrescue/contracts/moonCatUtils'
import { MOONCAT_COLLECTION } from './moonCatUtils'
import { FieldValue } from 'firebase-admin/firestore'

/**
 * Update dependent documents for Deposit Events
 *
 * Called when a Deposit Event from the JumpPort contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postDeposit: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { owner, tokenAddress, tokenId } = evtLog.args as {
    owner: HexString
    tokenAddress: HexString
    tokenId: number
  }

  if (tokenAddress == MoonCatAcclimator.address) {
    // MoonCat got deposited into the JumpPort
    const rescueOrder = tokenId
    const snapshot = await dbtx.get(db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', rescueOrder).limit(1))
    if (snapshot.empty) {
      logger.error(`Deposit event seen for MoonCat ${rescueOrder}, but no trait document exists for that MoonCat`)
      return
    }
    const mc = snapshot.docs[0]
    const moonCatData = mc.data()
    const logPreamble = `Deposit event seen for MoonCat ${mc.id}`
    const { shouldUpdateValue, shouldUpdateModified, shouldClearChecked } = shouldUpdateBlockchainMoment(
      moonCatData.owner[JumpPort.address],
      evtLog
    )
    if (!shouldUpdateValue && !shouldUpdateModified && !shouldClearChecked) {
      logger.debug(
        `${logPreamble}, owned by ${owner}, but no update needed for that: ${JSON.stringify(
          moonCatData.owner[JumpPort.address]
        )}`
      )
      return
    }

    // Assemble an object for feeding into a Firestore "update" call, for this change
    let updateMoonCat: { [key: string]: any } = {}
    const eventModified: BlockchainMoment = {
      blockHeight: evtLog.blockNumber,
      timestamp: evtLog.timestamp,
      txHash: evtLog.tx.hash,
    }
    if (shouldClearChecked) updateMoonCat[`owner.${JumpPort.address}.checked`] = FieldValue.delete()
    if (shouldUpdateValue) updateMoonCat[`owner.${JumpPort.address}.value`] = owner
    if (shouldUpdateModified) updateMoonCat[`owner.${JumpPort.address}.modified`] = eventModified
    await dbtx.update(mc.ref, updateMoonCat)
    if (shouldUpdateValue) {
      logger.info(`${logPreamble}, owned by ${owner}. MoonCat metadata updated.`)
    } else if (shouldUpdateModified) {
      logger.debug(`${logPreamble}, owned by ${owner}. Updating last-modified record.`)
    }
  }
}

/**
 * Update dependent documents for Withdraw Events
 *
 * Called when a Withdraw Event from the JumpPort contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postWithdraw: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { tokenAddress, tokenId } = evtLog.args as {
    tokenAddress: HexString
    tokenId: number
  }

  if (tokenAddress == MoonCatAcclimator.address) {
    // MoonCat got withdrawn from the JumpPort
    const rescueOrder = tokenId
    const snapshot = await dbtx.get(db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', rescueOrder).limit(1))
    if (snapshot.empty) {
      logger.error(`Withdraw event seen for MoonCat ${rescueOrder}, but no trait document exists for that MoonCat`)
      return
    }
    const mc = snapshot.docs[0]
    const moonCatData = mc.data()
    const logPreamble = `Withdraw event seen for MoonCat ${mc.id}`
    const { shouldUpdateValue, shouldUpdateModified, shouldClearChecked } = shouldUpdateBlockchainMoment(
      moonCatData.owner[JumpPort.address],
      evtLog
    )
    if (!shouldUpdateValue && !shouldUpdateModified && !shouldClearChecked) {
      logger.debug(
        `${logPreamble}, but no update needed for that: ${JSON.stringify(
          moonCatData.owner[JumpPort.address]
        )}`
      )
      return
    }

    // Assemble an object for feeding into a Firestore "update" call, for this change
    let updateMoonCat: { [key: string]: any } = {}
    const eventModified: BlockchainMoment = {
      blockHeight: evtLog.blockNumber,
      timestamp: evtLog.timestamp,
      txHash: evtLog.tx.hash,
    }
    if (shouldClearChecked) updateMoonCat[`owner.${JumpPort.address}.checked`] = FieldValue.delete()
    if (shouldUpdateValue) updateMoonCat[`owner.${JumpPort.address}.value`] = false
    if (shouldUpdateModified) updateMoonCat[`owner.${JumpPort.address}.modified`] = eventModified
    await dbtx.update(mc.ref, updateMoonCat)
    if (shouldUpdateValue) {
      logger.info(`${logPreamble}. MoonCat metadata updated.`)
    } else if (shouldUpdateModified) {
      logger.debug(`${logPreamble}. Updating last-modified record.`)
    }
  }
}
