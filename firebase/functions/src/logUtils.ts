import { Timestamp } from 'firebase-admin/firestore'
import { ethers } from 'ethers'
import { logger } from 'firebase-functions'
import {
  MoonCatSummary,
  postMoonCatRescued,
  postMoonCatNamed,
  postMoonCatAdopted,
  postMoonCatTransferred,
} from './moonCatUtils'
import { postDeposit, postWithdraw } from './jumpPortUtils'
import { HexString, BlockchainMoment, isHexString, BlockchainValue } from './types'
import { JumpPort, MoonCatAcclimator, MoonCatRescue } from '@mooncatrescue/contracts/moonCatUtils'

export const CONFIG_COLLECTION = 'app-config'
export const CONFIG_DOC = 'events-cache-heights'
export const EVENTS_COLLECTION = 'event-logs'
export const ADDR_META_COLLECTION = 'address-meta'

export type EventUpdateListener = (
  db: FirebaseFirestore.Firestore,
  dbtx: FirebaseFirestore.Transaction,
  evtLog: EventLog
) => void
export interface EventLog {
  blockNumber: number
  timestamp: Timestamp
  tx: {
    hash: HexString
    from: HexString
    to: HexString
    data: HexString
    value: string
  }
  logIndex: number
  eventName: string
  eventContract: HexString
  args: { [key: string]: any }
}

export interface TaskRunLog {
  taskName: string
  startTime: Timestamp
  startBlock: number
  endTime: Timestamp
  endBlock: number
  logsProcessed: number
  logsUpdated: number
}

export interface AddressMeta {
  ownedMoonCats: {
    list: MoonCatSummary[]
    count: number
    modified: Timestamp
  }
  lease?: Timestamp
}

/**
 * Take an Event and add a Firebase record for it
 */
export async function processEventLog(
  db: FirebaseFirestore.Firestore,
  contract: ethers.Contract,
  log: ethers.Event
): Promise<{ didUpdate: boolean; doc: FirebaseFirestore.DocumentReference }> {
  const collection = db.collection(EVENTS_COLLECTION)
  const docRef = collection.doc(`${log.transactionHash}-${log.logIndex}`)

  if (!isHexString(contract.address)) {
    logger.error(`Invalid Contract at ${contract.address}`)
    return { didUpdate: false, doc: docRef }
  }

  return await db.runTransaction(async (dbtx) => {
    // Check if this event exists
    if ((await dbtx.get(docRef)).exists) return { didUpdate: false, doc: docRef }

    let event = contract.interface.parseLog(log)
    let block = await log.getBlock()
    let tx = await log.getTransaction()

    // Convert the event args to just the named properties
    let args: { [key: string]: any } = {}
    for (let key in event.args) {
      if (parseInt(key) != Number(key)) {
        let value = event.args[key]
        if (ethers.BigNumber.isBigNumber(value)) {
          try {
            args[key] = value.toNumber()
          } catch (err: any) {
            if (err.code == 'NUMERIC_FAULT') {
              args[key] = value.toString()
            } else {
              logger.error('Error parsing BigNumber', value, err)
            }
          }
        } else {
          args[key] = value
        }
      }
    }

    if (
      !isHexString(log.transactionHash) ||
      !isHexString(tx.from) ||
      !isHexString(tx.to) ||
      !isHexString(tx.data) ||
      typeof log.event == 'undefined'
    ) {
      logger.error('Invalid raw log', log)
      return { didUpdate: false, doc: docRef }
    }

    const evtLog: EventLog = {
      blockNumber: block.number,
      timestamp: Timestamp.fromMillis(block.timestamp * 1000),
      tx: {
        hash: log.transactionHash,
        from: tx.from,
        to: tx.to,
        data: tx.data,
        value: tx.value.toString(),
      },
      logIndex: log.logIndex,
      eventName: log.event,
      eventContract: contract.address as HexString,
      args: args,
    }

    // Firestore transactions need to do all the data-reading first, before the data-writing
    // Update dependent documents
    const eventKey = `${evtLog.eventContract}-${evtLog.eventName}`
    switch (eventKey) {
      case `${MoonCatRescue.address}-CatRescued`:
        await postMoonCatRescued(db, dbtx, evtLog)
        break
      case `${MoonCatRescue.address}-CatNamed`:
        await postMoonCatNamed(db, dbtx, evtLog)
        break
      case `${MoonCatRescue.address}-CatAdopted`:
        await postMoonCatAdopted(db, dbtx, evtLog)
        break
      case `${MoonCatAcclimator.address}-Transfer`:
        await postMoonCatTransferred(db, dbtx, evtLog)
        break
      case `${JumpPort.address}-Deposit`:
        await postDeposit(db, dbtx, evtLog)
        break
      case `${JumpPort.address}-Withdraw`:
        await postWithdraw(db, dbtx, evtLog)
        break
      default:
        logger.debug(`No post-processing for ${eventKey}`)
        break
    }

    // Done reading updated info; write the updated info to the appropriate places
    // Save Event log as a record
    await dbtx.set(docRef, evtLog)
    logger.debug(`Created new Event record for ${log.transactionHash}-${log.logIndex}: ${eventKey}`)
    return { didUpdate: true, doc: docRef }
  })
}

/**
 * For a specific smart contract, grab all logged Events, working backward from current.
 *
 * Uses the 'task-logs' Firebase collection to keep one document per task execution to track metadata about it.
 */
export async function saveContractLogs({
  provider,
  db,
  taskName,
  contract,
}: {
  provider: ethers.providers.JsonRpcProvider
  db: FirebaseFirestore.Firestore
  taskName: string
  contract: ethers.Contract
}): Promise<{ ok: boolean; code: number; [key: string]: any }> {
  const startTime = new Date().getTime()
  const latestBlock = await provider.getBlock('latest')
  let endBlock = latestBlock.number
  const RUN_DELAY = 120 * 1000

  // Event queries for 2_000 blocks are guaranteed to succeed. We start larger than that, and if there's too many events
  // keep cutting the range in half. This function goes no further back than 20_000 blocks (about 2.7 days) as it's
  // primary purpose is a catch-up function; backfilling old events should be handled by other back-end processes
  const CHUNK_SIZE = 20_000
  const START_BLOCK = endBlock - CHUNK_SIZE

  // Check last run time
  let lastLog = await db
    .collection('task-logs')
    .where('taskName', '==', taskName)
    .orderBy('startTime', 'desc')
    .limit(1)
    .get()
  if (!lastLog.empty) {
    if (lastLog.docs[0].get('startTime').toMillis() + RUN_DELAY > startTime) {
      return { ok: false, code: 400, error: 'Too soon' }
    }
  }

  const config = await db.collection(CONFIG_COLLECTION).doc(CONFIG_DOC).get()
  let logsProcessed = 0
  let logsUpdated = 0

  while (true) {
    // Start the log entry, to mark task as 'running'
    let docRef = db.collection('task-logs').doc()
    const log: Partial<TaskRunLog> = {
      taskName: taskName,
      startTime: Timestamp.now(),
      endBlock: endBlock,
    }
    await docRef.set(log)
    logger.debug(`Started task ID ${docRef.id}`)

    const eventFilter: ethers.EventFilter = {
      address: contract.address,
      topics: [],
    }

    // Try fetching logs in smaller and smaller chunks until it succeeds
    let logs: false | ethers.Event[] = false
    let strikes = 0
    let chunkSize = CHUNK_SIZE
    do {
      try {
        logs = await contract.connect(provider).queryFilter(eventFilter, endBlock - chunkSize, endBlock)
      } catch (err) {
        strikes++
        chunkSize = Math.floor(chunkSize / 2)
        if (chunkSize < 2000) chunkSize = 2000
      }
    } while (strikes <= 10 && logs === false)
    if (logs === false) {
      logger.error(`Failed to fetch events from ${contract} from block ${endBlock} (chunk size ${chunkSize})`)
      return { ok: false, code: 500, logsProcessed, logsUpdated, endBlock }
    }
    logger.debug(`Event task ${docRef.id}: Found ${logs.length} logs from ${endBlock - chunkSize} to ${endBlock}`)

    // Start from the beginning of the log list and work toward the end
    // This is a subset of logs, taken from recent blocks, but we need to process in order,
    // so that logs that get processed last can still assume they're "the most recent" if block numbers are the same
    for (let i = 0; i < logs.length; i++) {
      const log = logs[i]
      if (typeof log.event == 'undefined') {
        logger.error(`Event task ${docRef.id}: log found with no event name`, log)
        logger.error('Known contract events: ' + Object.keys(contract.interface.events).join(', '))
        continue
      }
      const maxHeight = config.get(`${contract.address}-${log.event}`)
      if (!maxHeight) {
        logger.debug(
          `Event task ${docRef.id}: Event ${log.event} found from contract ${contract.address}, but that event isn't tracked by this app; skipping`
        )
      } else if (log.blockNumber >= maxHeight) {
        let rs = await processEventLog(db, contract, log)
        if (rs.didUpdate) logsUpdated++
        logsProcessed++
      } else {
        logger.debug(
          `Event task ${docRef.id}: Event ${log.event} from ${log.blockNumber} is below high-water mark of ${maxHeight} for that event type; skipping`
        )
      }

      if (i > 0 && i % 10 == 0) {
        // Check if we should end now
        if (new Date().getTime() - startTime >= 500 * 1000) {
          logger.debug(`Event task ${docRef.id}: Timeout imminent; closing out after ${i} of ${logs.length}`)

          // Update the task info
          const logEnd: Partial<TaskRunLog> = {
            endTime: Timestamp.now(),
            startBlock: log.blockNumber,
            logsProcessed,
            logsUpdated,
          }
          await docRef.set(logEnd, { merge: true })

          return { ok: true, code: 200, logsSeen: i, logsProcessed, logsUpdated, block: log.blockNumber }
        }
      }
    }

    // Out of logs; Save log result
    const logEnd: Partial<TaskRunLog> = {
      endTime: Timestamp.now(),
      startBlock: endBlock - chunkSize,
      logsProcessed,
      logsUpdated,
    }
    await docRef.set(logEnd, { merge: true })

    const secondsRunning = Math.floor((new Date().getTime() - startTime) / 1000)
    const batchStart = endBlock - chunkSize
    const logsSeen = logs.length
    logger.debug(
      `Event task ${docRef.id}: Finished after ${secondsRunning} seconds. ${logsSeen} logs between blocks ${batchStart} and ${endBlock}.`
    )
    if (secondsRunning >= 300) {
      // Not enough time for another batch of logs
      logger.debug(`Event task ${docRef.id}: Timeout imminent; closing out`)
      return { ok: true, code: 200, logsSeen, logsProcessed, logsUpdated, block: batchStart }
    }

    // There's still time available; do another chunk of logs
    endBlock = batchStart
    if (endBlock <= START_BLOCK) {
      logger.debug(`Event task ${docRef.id}: Hit high-water mark; closing out`)
      return { ok: true, code: 200, logsSeen, logsProcessed, logsUpdated, block: batchStart }
    }
  }
}

/**
 * Determine which parts of a recorded BlockchainValue should be updated for an incoming Event
 *
 * Each Event has a BlockchainMoment it happened in. This function checks the optional "checked" and "modified" values
 * on the BlockchainValue to see if they're present and newer/older than the incoming Event. This function outputs
 * a set of flags that indicate how the BlockchainValue should be updated.
 */
export function shouldUpdateBlockchainMoment(existing: BlockchainValue, newEvent: EventLog) {
  // Determine which parts of the record should be updated
  let shouldUpdateValue = false
  let shouldUpdateModified = false
  let shouldClearChecked = false

  // Should the "checked" time be cleared?
  if (existing.checked && existing.checked.blockHeight < newEvent.blockNumber) {
    // Document has a noted "checked" time, and that time is in the past;
    // this new event should supercede that past check
    shouldClearChecked = true
  }

  // Should the "modified" time be updated?
  if (!existing.modified) {
    // Document does not have a "modified" field
    shouldUpdateModified = true
  } else if (existing.modified.blockHeight <= newEvent.blockNumber) {
    // This assumes each batch of Events is processed oldest-to-newest; "<=" is used instead of "<" because
    // if they are equal, that's the same block, but this event must be later in the block, so is "more-recent".
    // Existing "modified" time is in the past; this new event should supercede that past modification
    shouldUpdateModified = true
  }

  // Should new "value" be noted?
  if (!existing.checked) {
    // Document does not have a "last-checked" time
    shouldUpdateValue = true
  } else if (existing.checked.blockHeight < newEvent.blockNumber) {
    // Document has a "last-checked" time, but that checked time is older than the incoming event
    shouldUpdateValue = true
  }

  return { shouldUpdateValue, shouldUpdateModified, shouldClearChecked }
}

/**
 * From a list of recorded BlockchainValues, get the checked or modified moment that's most-recent
 */
export function getLatestMoment(moments: Array<BlockchainValue | null>) {
  let maxMoment: BlockchainMoment = { blockHeight: 0 }
  for (let moment of moments) {
    if (moment && moment.checked && moment.checked.blockHeight > maxMoment.blockHeight) {
      maxMoment = moment.checked
    }
    if (moment && moment.modified && moment.modified.blockHeight > maxMoment.blockHeight) {
      maxMoment = moment.modified
    }
  }
  if (maxMoment.blockHeight == 0) return false
  return maxMoment
}
