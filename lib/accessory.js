require('dotenv').config();
const { API_URL } = process.env;
const LibMoonCat = require('libmooncat');
const Cache = require('./cache');

const CACHE_TYPE = 'accessory';

class Accessory {
  constructor(accessoryId) {
    this.accessoryId = accessoryId;
    this.data = {};
    this.eligibleListActive = null;
    this.eligibleRescueOrders = [];
    this.metadata = {};
    this.poses = [];
  }

  async init() {
    console.log(`Initialising Accessory Data: ${this.accessoryId}`);
    try {
      this.data = await LibMoonCat.getAccessory(API_URL, this.accessoryId);
      this.eligibleListActive = LibMoonCat.isEligibleListActive(
        this.data.eligibleList
      );
      this.eligibleRescueOrders = LibMoonCat.eligibleListToRescueOrders(
        this.data.eligibleList
      );
      this.metadata = this._parseMetaByte(this.data.meta);

      let poses = ['standing', 'sleeping', 'pouncing', 'stalking'];
      for (let i in this.data.positions) {
        if (JSON.stringify(this.data.positions[i]) !== '[255,255]')
          this.poses.push(poses[i]);
      }
    } catch (err) {
      console.log('Error in Accessory.init()', err);
      throw new Error('Error initialising Accessory!');
    }
  }

  getDrawable(paletteIndex, zIndex) {
    if (Object.keys(this.data).length === 0) return false;

    return {
      accessoryId: String(this.accessoryId),
      paletteIndex,
      zIndex,
      positions: this.data.positions,
      palettes: this.data.palettes,
      meta: this.data.meta,
      width: this.data.width,
      height: this.data.height,
      idat: this.data.idat,
    };
  }

  getDisplayName() {
    return `#${this.accessoryId} "${this.data.name}"`;
  }

  getSummary(zIndex) {
    return {
      accessoryId: String(this.accessoryId),
      name: this.data.name,
      displayName: this.getDisplayName(),
      visible: zIndex !== 0,
    };
  }

  isBackground() {
    return this.metadata.background;
  }

  getTrait() {
    return {
      trait_type: 'Accessory',
      value: `#${this.accessoryId}: ${this.data.name}`,
    };
  }

  getFullTraits() {
    return {
      palettes: this.data.palettes,
      meta: this.data.meta,
      eligibleCount: 25440 - this.eligibleRescueOrders.length,
      idat: this.data.idat,
      name: this.data.name,
      width: this.data.width,
      manager: this.data.manager,
      audience: this.metadata.audience,
      poses: this.poses,
      eligibleList:
        this.eligibleRescueOrders.length > 0 ? this.data.eligibleList : null,
      eligibleRescueOrders:
        this.eligibleRescueOrders.length > 0 ? this.eligibleRescueOrders : null,
      availablePalettes: this.data.availablePalettes,
      metadata: this.metadata,
      verified: this.metadata.verified,
      positions: this.data.positions,
      height: this.data.height,
      eligibleListActive: this.eligibleListActive,
    };
  }

  _parseMetaByte(b, incAudienceString) {
    incAudienceString = incAudienceString || false;
    const s = ('00000000' + b.toString(2)).slice(-8);
    const reserved = parseInt(s.slice(1, 3), 2);
    const audience = parseInt(s.slice(3, 5), 2);
    let metadata = {
      verified: s[0] == '1',
      reserved: reserved,
      audience: audience,
      mirrorPlacement: s[5] == '1',
      mirrorAccessory: s[6] == '1',
      background: s[7] == '1',
    };
    if (incAudienceString)
      metadata.audienceString = ['Everyone', 'Teen', 'Mature', 'Adult'][
        audience
      ];
    return metadata;
  }

  getRandomCatId() {
    let catId = null;

    if (this.eligibleListActive && this.eligibleRescueOrders.length > 0) {
      while (catId === null) {
        let randIndex = Math.floor(
          Math.random() * this.eligibleRescueOrders.length
        );
        let mc = LibMoonCat.getTraits(
          'basic',
          this.eligibleRescueOrders[randIndex]
        );
        if (this.poses.includes(mc.pose)) catId = mc.catId;
      }
    } else catId = LibMoonCat.randomMoonCatId({ poses: this.poses });

    return catId;
  }

  static factory(accessoryId) {
    if (!Cache.isFresh(CACHE_TYPE, accessoryId) && !Cache.isPending(CACHE_TYPE, accessoryId)) {
      Cache.addItemAsync(CACHE_TYPE, accessoryId, new Promise(async (resolve, reject) => {
        try {
          let acc = new Accessory(accessoryId);
          await acc.init();
          resolve(acc);
        } catch (err) {
          reject(err);
        }
      }));
    }

    return Cache.getItem(CACHE_TYPE, accessoryId);
  }

  static getAge(catId) {
    return Cache.itemAge(CACHE_TYPE, catId);
  }
}

module.exports = Accessory;
