require('dotenv').config();
const { API_URL, BASE_URL, CONFIG_FILENAME } = process.env;
const LibMoonCat = require('libmooncat');
const Cache = require('./cache');
const Accessory = require('./accessory');
const Lootprint = require('./lootprint');
const DynamicView = require('./dynamic');
const Config = require('../' + CONFIG_FILENAME);
const crypto = require('crypto');
const { ethers } = require('ethers');
const fs = require('fs').promises;

const provider = new ethers.providers.JsonRpcProvider(API_URL);
const JUMPPORT = new ethers.Contract(
  '0xF4d150F3D03Fa1912aad168050694f0fA0e44532',
  [
    'function ownerOf (address tokenAddress, uint256 tokenId) external view returns (address owner)',
  ],
  provider
);

const CACHE_TYPE = 'mooncat';
const IMAGES_CACHE = './img-cache';

// When a MoonCat's data is uncached, remove the images related to that MoonCat too
let moonCatImageMap = {};
function _purgeImages(catId) {
  if (typeof moonCatImageMap[catId] !== 'undefined') {
    for (const imgHash of moonCatImageMap[catId]) {
      let filename = `${IMAGES_CACHE}/${imgHash.substring(
        0,
        2
      )}/${imgHash}.json`;
      try {
        fs.unlink(filename);
      } catch (err) {
        if (err.code == 'ENOENT') {
          // File doesn't exist; no error
        } else {
          console.error(`Failed to delete cache file ${filename}`, err);
        }
      }
    }
  }
  moonCatImageMap[catId] = [];
}

// For a given scale, what should the glow size be?
// For small scales, these are hard-coded to good-looking values,
// while larger scales are algorithmic
const defaultGlow = {
  1: 3,
  2: 5,
  3: 8,
  4: 11,
};

class MoonCat {
  constructor(catId) {
    this.catId = catId;
    this.data = {};
    this.totalAccessories = 0;
    this.accessories = {};
    this.lootprint = null;

    this.rescueOrder = LibMoonCat.getRescueOrder(this.catId);
    this.traits = LibMoonCat.getTraits('erc721', this.catId);
  }

  async init() {
    console.log('Initialising MoonCat', this.catId);
    if (this.rescueOrder == null) {
      // Not a rescued MoonCat; nothing to initialize
      return;
    }

    try {
      this.data = await LibMoonCat.getContractDetails(API_URL, this.catId);

      // Check if in the JumpPort
      if (this.data.owner == JUMPPORT.address.toLowerCase()) {
        this.data.owner = await JUMPPORT.ownerOf(
          LibMoonCat.contracts.MoonCatAcclimator.address,
          this.data.rescueOrder
        );
        this.data.contract.description = 'JumpPort';
        this.data.contract.address = JUMPPORT.address;
      }

      // Add Accessories
      this.accessories = {};
      this.totalAccessories = await LibMoonCat.getTotalMoonCatAccessories(
        API_URL,
        this.rescueOrder
      );

      for (let i = 0; i < this.totalAccessories; i++) {
        let acc = await LibMoonCat.getMoonCatAccessory(
          API_URL,
          this.rescueOrder,
          i
        );
        acc.accessoryId = Number(acc.accessoryId);

        Accessory.factory(Number(acc.accessoryId)).catch((err) => {
          // Silently drop these as they will be picked up when actively requested
        }); // Force load Accessory data into cache

        this.accessories[acc.accessoryId] = acc;
      }

      // Add lootprint metadata
      this.lootprint = await Lootprint.factory(this.rescueOrder);

      if (this.data.catName !== null) {
        // Used extended cache for named mooncats
        Cache.setItemTtl(CACHE_TYPE, this.catId, Config.cache['mooncat-named']);
      }
    } catch (err) {
      console.error('Error in Mooncat.init()', err);
      throw new Error('Error initialising MoonCat!');
    }
  }

  getName() {
    if (this.rescueOrder == null) {
      return `MoonCat ${this.catId}`;
    }
    let name = `MoonCat #${this.rescueOrder}: `;

    switch (this.isNamed()) {
      case 'Yes':
        name += this.data.catName;
        break;
      case 'No':
        name += this.catId;
        break;
      default:
        name += '�';
    }

    if (this.totalAccessories > 0) name += ' (accessorized)';

    return name;
  }

  isNamed() {
    if (this.data.catName === null) return 'No';

    if (Config.names.malformed.includes(this.rescueOrder))
      return 'Invalid UTF8';
    if (Config.names.profane.includes(this.rescueOrder)) return 'Redacted';

    return 'Yes';
  }

  getDescription() {
    const link = (this.rescueOrder == null) ? Config.urls.project : `${Config.urls.purrse}/${this.rescueOrder}`;
    return `An Adorable ${this.traits.description.replace(
      'MoonCat',
      `[MoonCat](${link})`
    )}.`;
  }

  getPattern() {
    return (
      this.traits.details.pattern.charAt(0).toUpperCase() +
      this.traits.details.pattern.substring(1)
    );
  }

  getHue() {
    return this.traits.details.hue === 'skyBlue'
      ? 'Sky Blue'
      : this.traits.details.hue.charAt(0).toUpperCase() +
          this.traits.details.hue.substring(1);
  }

  getTotalGlow() {
    return this.traits.details.glow.reduce((sum, g) => sum + g, 0);
  }

  getSaturation() {
    return this.traits.details.isPale ? 'Pale' : 'Normal';
  }

  async getTraits() {
    const identifier = (this.rescueOrder == null) ? this.catId : this.rescueOrder;
    let data = {
      animation_url: `${BASE_URL}/dynamic/${identifier}`,
      description: this.getDescription(),
      license: Config.urls.license,
      name: this.getName(),
      background_color: this.traits.background_color,
      image: `${BASE_URL}/cat-image/${identifier}`,
      details: {
        ...(await this._getDetails()),
        ...this.traits.details,
      },
      attributes: [
        ...this.traits.attributes,
        this._trait('Coat Pattern', this.getPattern()),
        this._trait('Coat Hue', this.getHue()),
        this._trait('Coat Saturation', this.getSaturation()),
        this._trait('isNamed', this.isNamed()),
        ...this._getLootprint(),
        this._numericTrait('Hue', this.traits.details.hueValue, 359),
        //this._numericTrait('Total Accessories', this.totalAccessories),
        //...(await this._getAccessoryAttributes()),
        ...this._getMoments(),
        ...this._getSpokescat(),
      ],
    };
    if (this.rescueOrder != null) {
      data.external_url = `${Config.urls.purrse}/${this.rescueOrder}`;
    }
    return data;
  }

  async getContractDetails() {
    return {
      rescueIndex: this.rescueOrder,
      catId: this.catId,
      ...(await this._getDetails()),
    };
  }

  async getDynamicHtml() {
    let accessories = [];

    for (const accId in this.accessories) {
      let acc = await Accessory.factory(Number(accId));
      accessories.push({
        accessoryId: Number(accId),
        name: acc.data.name,
        zIndex: this.accessories[accId].zIndex,
        verified: acc.metadata.verified,
      });
    }

    accessories.sort((a, b) => a.accessoryId - b.accessoryId);

    return DynamicView(this.catId, this.getTotalGlow(), accessories);
  }

  ownsAccessory(accId) {
    return (typeof this.accessories[accId] != 'undefined')
  }

  async _getDetails() {
    return {
      name: this.data.catName,
      isAcclimated: this.data.isAcclimated,
      owner: this.data.owner,
      lootprint:
        this.lootprint !== null && this.lootprint.hasLootprint()
          ? 'Claimed'
          : 'Unclaimed',
      contract: this.data.contract,
      accessories: await this._getAccessories(),
    };
  }

  async _getAccessories() {
    let accessories = [];

    for (const accId in this.accessories) {
      let acc = await Accessory.factory(Number(accId));
      accessories.push(acc.getSummary(this.accessories[accId].zIndex));
    }

    return accessories;
  }

  async _getAccessoryAttributes() {
    let attributes = [];

    for (const accId in this.accessories) {
      let acc = await Accessory.factory(Number(accId));
      attributes.push(acc.getTrait());
    }

    return attributes;
  }

  _getLootprint() {
    return this.lootprint !== null ? [this.lootprint.getTrait()] : [];
  }

  _getMoments() {
    let moments = [];

    for (let i = 0; i < Config.moments.length; i++) {
      if (Config.moments[i].cats.includes(this.rescueOrder))
        moments.push(
          this._trait('In Moment', `${i}: ${Config.moments[i].name}`)
        );
    }
    return moments;
  }

  _getSpokescat() {
    let spokescat = [];

    if (Object.keys(Config.spokescats).includes(String(this.rescueOrder))) {
      let vm = Config.spokescats[this.rescueOrder];
      spokescat.push(this._trait('MoonCatPop SpokesCat', 'Yes'));
      spokescat.push(this._trait('SpokesCat For', `${vm.id}: ${vm.flavor}`));
    }

    return spokescat;
  }

  /**
   * Create a numeric trait object for OpenSea-style metadata.
   * https://docs.opensea.io/docs/metadata-standards#attributes
   * Sets the `display_type` property to "number", to have OpenSea show as a bare number,
   * rather than a "property" of the asset.
   */
  _numericTrait(name, value, max) {
    let trait = {
      display_type: 'number',
      trait_type: name,
      value,
    };
    if (typeof max !== 'undefined') trait.max_value = max;
    return trait;
  }

  /**
   * Create a standard trait object for OpenSea-style metadata.
   * https://docs.opensea.io/docs/metadata-standards#attributes
   */
  _trait(name, value, max) {
    let trait = {
      trait_type: name,
      value,
    };
    if (typeof max !== 'undefined') trait.max_value = max;
    return trait;
  }

  /**
   * Create a unique identifier for a specific image request.
   * Given a set of sanitized options passed in from the user request, create a unique identifier for that request
   */
  _imgRequestHash(options) {
    let tmp = { id: this.catId };
    Object.keys(options)
      .sort((a, b) => a.localeCompare(b))
      .forEach(k => {
        tmp[k] = options[k];
      });
    let hash = crypto.createHash('md5').update(JSON.stringify(tmp)).digest('hex');
    return hash;
  }

  async getImageData(options) {
    options = options || {};
    options.scale = Number(options.scale);
    options.padding = Number(options.padding);
    if (options.scale > 20) options.scale = 20;
    if (options.scale < 1) options.scale = 1;
    if (options.padding > 200) options.padding = 200;
    if (options.padding < 0) options.padding = 0;
    if (typeof options.acc == 'function') options.acc = options.acc(this)

    // How big should the glow be?
    if (typeof options.glowSize !== 'undefined') {
      options.glowSize = Number(options.glowSize); // Allow the request to override with a specific value
    } else {
      if (options.scale < 5) {
        options.glowSize = defaultGlow[options.scale]; // Small scales need precise values to look their best
      } else {
        options.glowSize = 12 + (options.scale - 5);
      }
    }

    if (typeof options['acc-inc'] == 'undefined') options['acc-inc'] = [];
    if (typeof options['acc-exc'] == 'undefined') options['acc-exc'] = [];

    // Should the glow be visible at all?
    if (typeof options.glow === 'undefined') {
      options.glow = this.data.isAcclimated; // If unset, set to Acclimation status
    } else {
      options.glow = (options.glow == 'true'); // Force to be boolean
    }

    options.costumes = options.costumes === 'true'; // Force to be boolean

    // Which accessories count as 'costumes' (and default to unrendered)?
    let costumes = Config.accessories.costumes;
    if (options['acc-inc'].length > 0) {
      // If the request explicitly asks for a specific costume accessory, don't turn it off
      costumes = costumes.filter(id => !options['acc-inc'].includes(Number(id)));
    }

    // Parse accessories into single list
    let accessoriesToShow = {};
    if (typeof options.acc !== 'undefined') {
      // The request has a list of accessories; make sure they're valid
      accessoriesToShow = options.acc.split(',').reduce((rs, reqAcc, index) => {
        let [accId, accPalette] = reqAcc.split(':').map(n => parseInt(n));
        if (isNaN(accId)) return rs; // Not a valid accessory identifier
        if (typeof this.accessories[accId] == 'undefined') return rs; // Not owned by this MoonCat

        if (typeof accPalette == 'undefined' || isNaN(accPalette)) {
          // Request did not specify a palette; use the last-set on-chain palette preference
          accPalette = this.accessories[accId].paletteIndex;
        }
        // Add this accessory to the result
        rs[accId] = {
          paletteIndex: accPalette,
          zIndex: index + 1,
        };
        return rs;
      }, {});
    } else {
      // The request does not have an explicit list of accessories; use the on-chain worn accessories of this MoonCat
      for (const accId in this.accessories) {
        let curAcc = this.accessories[accId];
        let acc = await Accessory.factory(Number(curAcc.accessoryId));
        if (!acc.metadata.verified) continue; // Skip unverified accessories
        if (typeof options.noBackground !== 'undefined' && acc.isBackground()) continue; // Skip background accessories if requested
        if (options['acc-exc'].includes(Number(accId))) continue; // Skip accessories that are explicitly excluded in the request
        if (curAcc.zIndex === 0) continue; // Skip accessories that are off in the on-chain configuration
        if (costumes.includes(Number(accId)) && options.costumes !== true) continue; // Skip costumes unless the request specifies
        if (options['acc-inc'].length > 0 && !options['acc-inc'].includes(Number(accId))) continue; // If an inclusion list is specified, skip if this accessory isn't in it

        accessoriesToShow[accId] = {
          paletteIndex: curAcc.paletteIndex,
          zIndex: curAcc.zIndex,
        };
      }
    }

    // Sanitize options
    delete options['acc-inc'];
    delete options['acc-exc'];
    options.acc = accessoriesToShow;

    let imgHash = this._imgRequestHash(options);

    // See if cached image exists
    let cacheFile = IMAGES_CACHE + '/' + imgHash.substring(0, 2) + '/' + imgHash + '.json';
    try {
      let stats = await fs.stat(cacheFile);
      let modifiedDelta = (Date.now() - stats.mtimeMs) / 1000;
      if (modifiedDelta <= Config.cache['accessorized-image']) {
        console.log('Cache hit', imgHash, modifiedDelta + ' secs old');
        let fileData = await fs.readFile(cacheFile);
        let imgData = JSON.parse(fileData.toString('utf-8'));
        imgData.age = parseInt(modifiedDelta);
        return imgData;
      }
    } catch (err) {
      // Cache file doesn't exist; ignore
    }

    try {
      let accessories = [];
      for (const accId in options.acc) {
        let acc = await Accessory.factory(
          Number(accId)
        );
        accessories.push(acc.getDrawable(
          options.acc[accId].paletteIndex,
          options.acc[accId].zIndex
        ));
      }

      let imgData = LibMoonCat.generateImageWithDimensions(
        this.catId,
        accessories,
        options
      );
      imgData.hash = imgHash;
      imgData.catId = this.catId;
      imgData.options = JSON.stringify(options);

      // Save cache file
      try {
        await fs.mkdir(IMAGES_CACHE);
      } catch (err) {
        if (err.code == 'EEXIST') {
          // Folder already existed; no error
        } else {
          return err;
        }
      }
      let subfolder = IMAGES_CACHE + '/' + imgHash.substring(0, 2);
      try {
        await fs.mkdir(subfolder);
      } catch (err) {
        if (err.code == 'EEXIST') {
          // Folder already existed; no error
        } else {
          return err;
        }
      }
      await fs.writeFile(subfolder + '/' + imgHash + '.json', JSON.stringify(imgData, null, 2));

      // Record this relation to the MoonCat
      if (typeof moonCatImageMap[this.catId] == 'undefined') {
        moonCatImageMap[this.catId] = [];
      }
      moonCatImageMap[this.catId].push(imgHash);

      // Return the new data
      imgData.age = 0;
      return imgData;
    } catch (err) {
      console.log(`Issue generating image for MoonCat ${this.rescueOrder}`);
      console.log(err);
    }
  }

  static factory(catId) {
    if (!Cache.isFresh(CACHE_TYPE, catId) && !Cache.isPending(CACHE_TYPE, catId)) {
      Cache.addItemAsync(CACHE_TYPE, catId, new Promise(async (resolve, reject) => {
        try {
          let mooncat = new MoonCat(catId);
          await mooncat.init();
          resolve(mooncat);
        } catch (err) {
          reject(err);
        }
      }));
      _purgeImages(catId);
    }

    return Cache.getItem(CACHE_TYPE, catId);
  }

  static getAge(catId) {
    return Cache.itemAge(CACHE_TYPE, catId);
  }
}

module.exports = MoonCat;
