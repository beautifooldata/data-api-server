openapi: 3.0.1
info:
  title: MoonCat Data
  description: API to serve data and images to support the MoonCat ecosystem
  version: 2023.03.06
servers:
  - url: https://api.mooncat.community
    description: Main (production) server
  - url: http://localhost:25690
    description: Development (local) server
paths:
  /traits/{catId}:
    get:
      tags:
        - Traits
      summary: Trait metadata for the requested MoonCat
      description: Full metadata used as the return URL for a TokenURI request on the Acclimated wrapper. This includes an `attributes` property that adheres to [OpenSea metadata standards](https://docs.opensea.io/docs/metadata-standards), and a `detials` property that is more MoonCat-specific in formatting.
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Traits'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /contract-details:
    options:
      tags:
        - Traits
      responses:
        '200':
          $ref: '#/components/responses/ContractOptions'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'
    post:
      tags:
        - Traits
      summary: Contract-related trait metadata for multiple MoonCats at once
      requestBody:
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  - $ref: '#/components/schemas/CatId'
                  - $ref: '#/components/schemas/RescueIndex'
      responses:
        '200':
          description: JSON array of contract details for the requested MoonCats
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ContractDetails'


  /contract-details/{catId}:
    get:
      tags:
        - Traits
      summary: Contract-related trait metadata for the requested MoonCat
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/ContractDetails'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'


  /image/{catId}:
    get:
      tags:
        - Images
      summary: Image in PNG format of requested MoonCat as returned by metadata
      description: This endpoint is highly-customizable and can return PNG renderings of MoonCats with and without Accessories, as well as head-only renderings.
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
        - in: query
          name: scale
          description: For each pixel of the MoonCat's design, how large should it be rendered on output
          schema:
            type: integer
            default: 3
            minimum: 1
            maximum: 20
        - in: query
          name: padding
          description: How much additional transparent padding should be added around the rendered image
          schema:
            type: integer
            default: 10
            minimum: 0
            maximum: 200
        - in: query
          name: backgroundColor
          description: What color to fill in behind the rendered image
          schema:
            type: string
            default: transparent
        - in: query
          name: glowSize
          schema:
            type: integer
            minimum: 0
            maximum: 20
        - in: query
          name: glowOpacity
          schema:
            type: number
            minimum: 0
            maximum: 1
        - in: query
          name: acc
          description: Of all the accessories owned by the MoonCat, only show these Accessories
          schema:
            type: array
            items:
              type: string
          style: form
          explode: false
        - in: query
          name: acc-inc
          description: Of all the accessories currently being worn by the MoonCat, only show these Accessories
          schema:
            type: array
            items:
              type: number
          style: form
          explode: false
        - in: query
          name: acc-exc
          description: Of all the accessories currently being worn by the MoonCat, don't show these Accessories
          schema:
            type: array
            items:
              type: number
          style: form
          explode: false
        - in: query
          name: fullSize
          schema:
            type: boolean
          allowEmptyValue: true
        - in: query
          name: noCat
          schema:
            type: boolean
          allowEmptyValue: true
        - in: query
          name: headOnly
          schema:
            type: boolean
          allowEmptyValue: true
        - in: query
          name: noBackground
          schema:
            type: boolean
          allowEmptyValue: true
        - in: query
          name: glow
          schema:
            type: boolean
          allowEmptyValue: true

      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /cat-image/{catId}:
    get:
      tags:
        - Images
      summary: Image in PNG format of the requested MoonCat by themselves
      description: PNG rendering of the MoonCat by itself, with no additional accessories or extras, and if they're not Acclimated, not glowing either
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /regular-image/{catId}:
    get:
      tags:
        - Images
      summary: Image in PNG format of the requested MoonCat in an original state
      description: PNG rendering of the MoonCat by itself, not glowing, with no additional accessories or extras.
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /glow-image/{catId}:
    get:
      tags:
        - Images
      summary: Image in PNG format of the requested MoonCat with an Acclimated glow
      description: PNG rendering of what this MoonCat looks like when Acclimated; it gains a glow around it, which has a color matching its core hex identifier color.
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /face-image/{catId}:
    get:
      tags:
        - Images
      summary: Image in PNG format of the requested MoonCat's face
      description: PNG rendering of this MoonCat's face, with no additional accessories or extras, and if they're not Acclimated, not glowing either.
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /accessorized-image/{catId}:
    get:
      tags:
        - Images
      summary: Image in PNG format of requested MoonCat with worn accessories
      description: PNG rendering of this MoonCat with any accessories its current owner has indicated on-chain that they want the MoonCat to be wearing.
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'
  
  /event-image/{catId}:
    get:
      tags:
        - Images
      summary: Image in PNG format of requested MoonCat wearing accessories for the current event
      description: PNG rendering of this MoonCat, wearing any accessories they own that are relevant to the current event/holiday being celebrated, even if they aren't configured as worn in the on-chain accessory choices.
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /cat-walk/{rescueIndex}:
    get:
      tags:
        - Images
      summary: Image in PNG format of requested MoonCat as a sprite sheet for 8-frame animation
      description: PNG rendering of this MoonCat as a sprite sheet of different walk cycles for the different cardinal directions, for use as an 8-frame animation.
      parameters:
        - $ref: '#/components/parameters/RescueIndex'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Image'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /owned-mooncats/{address}:
    get:
      tags:
        - Owners
      summary: List of Rescue Indexes owned by a given address
      parameters:
        - in: path
          name: address
          description: Ethereum Address
          required: true
          schema:
            $ref: '#/components/schemas/EthAddress'
      responses:
        '200':
          description: A JSON array containing MoonCats owned by given Address under each contract
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  title: MoonCat
                  properties:
                    rescueOrder:
                      $ref: '#/components/schemas/RescueIndex'
                    catId:
                      $ref: '#/components/schemas/CatId'
                    contract:
                      type: object
                      properties:
                        name:
                          type: string
                          enum: ['Original', 'Acclimated', 'JumpPort']
                        address:
                          $ref: '#/components/schemas/EthAddress'
                      required:
                        - name
                        - address
                  required:
                    - rescueOrder
                    - catId
                    - contract
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /accessory/{accessoryId}:
    get:
      tags:
        - Accessories
      summary: Details of the given Accessory
      parameters:
        - $ref: '#/components/parameters/AccessoryId'
      responses:
        '200':
          $ref: '#/components/responses/Accessory'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /accessory-image/{accessoryId}:
    get:
      tags:
        - Accessories
        - Images
      summary: Image in PNG format of requested Accessory (displayed on a random MoonCat)
      parameters:
        - $ref: '#/components/parameters/AccessoryId'
      responses:
        '200':    # status code
          $ref: '#/components/responses/AccessoryImage'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /dynamic/{catId}:
    get:
      tags:
        - HTML
      summary: An HTML page with functionality to toggle accessory images
      parameters:
        - $ref: '#/components/parameters/MoonCatIdentifier'
      responses:
        '200':    # status code
          $ref: '#/components/responses/Dynamic'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /costumes:
    get:
      tags:
        - Accessories
      summary: A list of accessories which have been marked as Costumes
      responses:
        '200':
          description: Returns an array of accessory ids
          content:
            application/javascript:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/AccessoryId'
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /events:
    get:
      tags:
        - Accessories
      summary: A list of Events that have custom imagery associated with them
      responses:
        '200':
          description: Returns an array of event objects
          content:
            application/javascript:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    label:
                      type: string
                    start:
                      type: string
                      format: date-time
                    end:
                      type: string
                      format: date-time

  /invalidate:
    post:
      tags:
        - Administration
      summary: Invalidate specific items in the cache
      parameters:
        - $ref: '#/components/parameters/CacheType'
        - $ref: '#/components/parameters/CacheKey'
      responses:
        '200':
          description: Invalidate a cached item
          content:
            text/plain:
              schema:
                type: string
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /lib/mooncatdata.js:
    get:
      tags:
        - Static
      summary: the libmooncat library
      description: Serves up a copy of the [libmooncat](https://www.npmjs.com/package/libmooncat) module.
      responses:
        '200':
          description: Returns the LibMoonCat JS
          content:
            text/plain:
              schema:
                type: string
                format: binary
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

  /lib/mooncatparser.js:
    get:
      tags:
        - Static
      summary: Returns the MoonCat Parser JS
      description: Serves up a copy of the `mooncatparser.js` file from the original 2017 project.
      responses:
        '200':
          description: Returns the MoonCat Parser JS
          content:
            text/plain:
              schema:
                type: string
                format: binary
        '424':
          $ref: '#/components/responses/TimeoutError'
        '429':
          $ref: '#/components/responses/RateLimitedError'

components:
  parameters:
    CatId:
      in: path
      name: catId
      description: A five-byte hex string, storing the base attributes of a MoonCat in a compressed form
      required: true
      schema:
        $ref: '#/components/schemas/CatId'
    RescueIndex:
      in: path
      name: rescueIndex
      description: A decimal number representing the index of the MoonCat in the collection (the order they entered the Rescue ship in)
      required: true
      schema:
        $ref: '#/components/schemas/RescueIndex'
    MoonCatIdentifier:
      in: path
      name: catId
      description: A unique identifier for a MoonCat; either a five-byte hex string, or a decimal index value
      required: true
      schema:
        oneOf:
          - $ref: '#/components/schemas/CatId'
          - $ref: '#/components/schemas/RescueIndex'
      examples:
        hex:
          summary: Hex ID
          value: '0x00d658d50b'
        rescue:
          summary: Rescue index
          value: 0
    AccessoryId:
      in: path
      name: accessoryId
      description: An Accessory ID
      required: true
      schema:
        $ref: '#/components/schemas/AccessoryId'
    CacheType:
      in: query
      name: type
      description: A Cache Type
      required: true
      schema:
        type: string
        enum: ['accessory', 'lootprint', 'mooncat']
    CacheKey:
      in: query
      name: key
      description: The ID of the item in the Cache
      required: true
      schema:
        type: integer
        example: 0
  schemas:
    CatId:
      type: string
      pattern: '^0x[0-9a-fA-F]{10}$'
      example: '0x00d658d50b'
    RescueIndex:
      type: integer
      minimum: 0
      maximum: 25439
      example: 0
    AccessoryId:
      type: integer
      minimum: 0
      example: 21
    EthAddress:
      type: string
      pattern: '^0x[0-9a-fA-F]{40}$'
      example: '0xdf2E60Af57C411F848B1eA12B10a404d194bce27'
    Hash32:
      type: string
      pattern: '^[0-9a-fA-F]{32}$'
    Bytes32:
      type: string
      pattern: '^0x[0-9a-fA-F]{64}$'
    Lootprint:
      type: string
      enum: ['Claimed']
    Accessory:
      type: object
      additionalProperties: false
      properties:
        accessoryId:
          type: integer
          minimum: 0
        name:
          type: string
        displayName:
          type: string
        visible:
          type: boolean
      required:
        - accessoryId
        - name
        - displayName
        - visible
    AccessoryDetails:
      type: object
      additionalProperties: false
      properties:
        palettes:
          type: array
          minItems: 1
          maxItems: 7
          items:
            type: array
            minItems: 1
            maxItems: 8
            items:
              type: integer
        meta:
          type: integer
        eligibleCount:
          type: integer
        idat:
          type: string
        name:
          type: string
        width:
          type: integer
        manager:
          $ref: '#/components/schemas/EthAddress'
        audience:
          type: integer
        poses:
          type: array
          items:
            type: string
            enum: ['standing', 'sleeping', 'pouncing', 'stalking']
        eligibleList:
          type: array
          nullable: true
          minItems: 100
          maxItems: 100
          items:
            $ref: '#/components/schemas/Bytes32'

        eligibleRescueOrders:
          type: array
          nullable: true
          items:
            $ref: '#/components/schemas/RescueIndex'
        availablePalettes:
          type: integer
        metadata:
          $ref: '#/components/schemas/Metadata'
        verified:
          type: boolean
        positions:
          type: array
          minItems: 4
          maxItems: 4
          items:
            type: array
            minItems: 2
            maxItems: 2
            items:
              type: integer
        height:
          type: integer
        eligibleListActive:
          type: boolean
      required:
        - palettes
        - meta
        - eligibleCount
        - idat
        - name
        - width
        - manager
        - audience
        - poses
        - availablePalettes
        - metadata
        - verified
        - positions
        - height
        - eligibleListActive
    Attribute:
      type: object
      additionalProperties: false
      properties:
        trait_type:
          type: string
        value:
          oneOf:
            - type: string
            - type: integer
        max_value:
          type: integer
      required:
        - trait_type
        - value
    Contract:
      type: object
      additionalProperties: false
      properties:
        tokenId:
          type: integer
        description:
          type: string
        address:
          $ref: '#/components/schemas/EthAddress'
        capabilities:
          type: array
          items:
            type: string
            enum: ['ERC20', 'ERC721', 'ERC998']
      required:
        - tokenId
        - description
        - address
        - capabilities
    ContractDetails:
      type: object
      additionalProperties: false
      properties:
        rescueIndex:
          $ref: '#/components/schemas/RescueIndex'
        catId:
          $ref: '#/components/schemas/CatId'
        name:
          type: string
          nullable: true
        isAcclimated:
          type: boolean
        owner:
          $ref: '#/components/schemas/EthAddress'
        lootprint:
          $ref: '#/components/schemas/Lootprint'
        contract:
          $ref: '#/components/schemas/Contract'
        accessories:
          type: array
          items:
            $ref: '#/components/schemas/Accessory'
      required:
        - rescueIndex
        - catId
        - name
        - isAcclimated
        - owner
        - lootprint
        - contract
        - accessories
    Details:
      type: object
      additionalProperties: false
      properties:
        hue:
          type: string
        kInt:
          type: integer
        onlyChild:
          type: boolean
        isPale:
          type: boolean
        litterId:
          $ref: '#/components/schemas/Hash32'
        catId:
          $ref: '#/components/schemas/CatId'
        expression:
          type: string
        cloneSet:
          type: array
          items:
            $ref: '#/components/schemas/RescueIndex'
        accessories:
          type: array
          items:
            $ref: '#/components/schemas/Accessory'
        name:
          type: string
          nullable: true
        contract:
          $ref: '#/components/schemas/Contract'
        facing:
          type: string
        hasTwins:
          type: boolean
        litter:
          type: array
          items:
            $ref: '#/components/schemas/RescueIndex'
        twinId:
          $ref: '#/components/schemas/Hash32'
        classification:
          type: string
        pale:
          type: boolean
        lootprint:
          $ref: '#/components/schemas/Lootprint'
        kBin:
          type: string
          pattern: '^[01]{8}$'
        hasClones:
          type: boolean
        twinSetSize:
          type: integer
        litterSize:
          type: integer
        mirrorSetSize:
          type: integer
        isAcclimated:
          type: boolean
        pose:
          type: string
        rescueIndex:
          $ref: '#/components/schemas/RescueIndex'
        rescuedBy:
          $ref: '#/components/schemas/EthAddress'
        cloneId:
          $ref: '#/components/schemas/Hash32'
        genesis:
          type: boolean
        twinSet:
          type: array
          items:
            $ref: '#/components/schemas/RescueIndex'
        rescueYear:
          type: integer
          enum: [2017, 2018, 2019, 2020, 2021]
        mirrorId:
          $ref: '#/components/schemas/Hash32'
        glow:
          type: array
          minItems: 3
          maxItems: 3
          items:
            type: integer
            minimum: 0
            maximum: 255
        owner:
          $ref: '#/components/schemas/EthAddress'
        mirrorSet:
          type: array
          items:
            $ref: '#/components/schemas/RescueIndex'
        hasMirrors:
          type: boolean
        cloneSetSize:
          type: integer
        pattern:
          type: string
        hueValue:
          type: integer
      required:
        - hue
        - kInt
        - onlyChild
        - isPale
        - litterId
        - catId
        - expression
        - cloneSet
        - accessories
        - contract
        - facing
        - hasTwins
        - litter
        - twinId
        - classification
        - pale
        - lootprint
        - kBin
        - hasClones
        - twinSetSize
        - litterSize
        - mirrorSetSize
        - isAcclimated
        - pose
        - rescueIndex
        - rescuedBy
        - cloneId
        - genesis
        - twinSet
        - rescueYear
        - mirrorId
        - glow
        - owner
        - mirrorSet
        - hasMirrors
        - cloneSetSize
        - pattern
        - hueValue
    Metadata:
      type: object
      additionalProperties: false
      properties:
        verified:
          type: boolean
        reserved:
          type: integer
        audience:
          type: integer
        mirrorPlacement:
          type: boolean
        mirrorAccessory:
          type: boolean
        background:
          type: boolean
      required:
        - verified
        - reserved
        - audience
        - mirrorPlacement
        - mirrorAccessory
        - background
    Traits:
      type: object
      additionalProperties: false
      properties:
        animation_url:
          type: string
          format: uri
        description:
          type: string
        external_url:
          type: string
          format: uri
        license:
          type: string
          format: uri
          enum: ['https://mooncat.community/mooncatlicense']
        name:
          type: string
          nullable: true
        background_color:
          type: string
          pattern: '^[0-9A-F]{6}$'
        details:
          $ref: '#/components/schemas/Details'
        image:
          type: string
          format: uri
        attributes:
          type: array
          items:
            $ref: '#/components/schemas/Attribute'
      required:
        - animation_url
        - description
        - external_url
        - license
        - name
        - background_color
        - details
        - image
        - attributes
  responses:
    ContractDetails:
      description: A JSON object containing contract related metadata
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ContractDetails'
    ContractOptions:
      description: OK
      headers:
        Access-Control-Allow-Origin:
          schema:
            type: string
            example: '*'
        Access-Control-Allow-Methods:
          schema:
            type: string
            example: 'POST, OPTIONS'
        Access-Control-Allow-Headers:
          schema:
            type: string
            example: 'Accept, Content-Type'
      content:
        text/plain:
          schema:
            type: string
            example: 'ok'
    RateLimitedError:
      description: Response given when the client is making too many requests
      content:
        application/json:
          schema:
            type: object
            properties:
              error:
                type: string
                enum: ['Rate limit reached', 'Rate limit reached; penalty enforced']
              nextAllowed:
                type: number
                format: timestamp

    TimeoutError:
      description: Response given when an upstream service has taken too long to respond
      content:
        application/json:
          schema:
            type: object
            properties:
              error:
                type: string
                enum: ['Timed out']
    Traits:
      description: A JSON object containing full MoonCat metadata
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Traits'
    Image:
      description: A PNG image of the MoonCat
      headers:
        Age:
          schema:
            type: integer
          description: The amount of seconds that have passed since this image was last generated
        Content-Length:
          schema:
            type: integer
          description: The file size of the image
        ETag:
          schema:
            type: string
          description: A unique identifier for this image
        X-Image-Width:
          schema:
            type: integer
          description: The width of the image being returned, in pixels
        X-Image-Height:
          schema:
            type: integer
          description: The height of the image being returned, in pixels
        X-Parsed-Options:
          schema:
            type: string
          description: JSON-formatted options object that was used to generate this image.
      content:
        image/png:
          schema:
            type: string
            format: binary
    Accessory:
      description: A JSON object containing full details of an Accessory
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/AccessoryDetails'
    AccessoryImage:
      description: A PNG visualization of an Accessory, visualized wit a random MoonCat who is eligible for it
      headers:
        Content-Length:
          schema:
            type: integer
          description: The file size of the image
    Dynamic:
      description: An HTML page used to provide OpenSea with functionality to toggle accessory images
      content:
        text/html:
          schema:
            type: string
            format: binary
